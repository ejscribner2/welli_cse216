<div id="Profiles" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Profile</h4>
            </div>
            <div class="modal-body">
          
                <label for="Profiles-name">Name: {{mData.mFirstname}} {{mData.mLastname}}</label>
                <label for="Profiles-myusername">Username: {{mData.mUsername}}</label>
                <label for="Profiles-email">Email: {{mData.mEmail}}</label>
                <label for="Profiles-Bio">Bio: {{mData.mBio}}</label>
          
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="Profiles-Close">Close</button>
            </div>
        </div>
    </div>
</div>