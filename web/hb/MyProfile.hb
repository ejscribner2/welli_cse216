<div id="MyProfile" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">My Profile</h4>
                <button type="button" class="close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

							<div class="card dropShadowFade">
								<div class="card-header">
									<div class="row" style="margin-bottom: -50px">
										<div class="col-md-9">
											<h3 style="margin-top: 40px; margin-bottom: 46px;">
												{{profileInfo.mData.mFirstname}} {{profileInfo.mData.mLastname}}
											</h3>
										</div>
										<div class="col-md-3">
											<div class="image-cropper">
												<img src="{{profileUrl}}" alt="avatar" class="profile-pic">
											</div>
										</div>
									</div>
								</div>
								<div class="card-body">
									<div class="row">
										<div class="col-md-6">
											<h5>Username: </h5>
											<hr class="line" width="21.6em">
											<p id="MyProfile-myusername">{{profileInfo.mData.mUsername}}</label>
										</div>
										<div class="col-md-6">
											<h5>Bio: </h5>
											<hr class="line" width="21.6em">
											<p id="MyProfile-Bio">{{profileInfo.mData.mBio}}</p>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-9 card-title">
											<input class="form-control" type="text" id="MyProfile-changeBio" placeholder="What would you like your bio to say?" />
										</div>
										<div class="col-md-3">
											<button type="button" class="btn btn-info" id="MyProfile-editBio" style="width: 100%">Update Bio</button>
										</div>
									</div>
								</div>
							</div>

							<br>

							<div class="card dropShadowFade">
								<div class="card-header">
									<h4>My Posts:</h4>
								</div>
								<div class="card-body">
									<table class="table table-hover">
										<thead>
										<tr>
											<th scope="col">Title/Location</th>
											<th scope="col">Body</th>
											<th scope="col">Upvotes</th>
											<th scope="col">Downvotes</th>
										</tr>
										</thead>
										<tbody>
										{{#each userPosts.mData}}
											<tr>
												<td>{{this.mTitle}}</td>
												<td>{{this.mBody}}</td>
												<td>{{this.mUpvotes}}</td>
												<td>{{this.mDownvotes}}</td>
											</tr>
										{{/each}}
										</tbody>
									</table>
								</div>
							</div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-info" id="MyProfile-editBio">Update Bio</button>
                <button type="button" class="btn btn-secondary" id="MyProfile-Close">Close</button>
            </div>
        </div>
    </div>
</div>
