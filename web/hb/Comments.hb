<div id="Comments" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Is it a good boy?</h4>
            </div>
            <div class="modal-body">
                <label for="Comments-message">Message</label>
                <textarea class="form-control" id="Comments-message" autofocus></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" id="Comments-SeeAll">See All</button>
                <button type="button" class="btn btn-success" id="Comments-OK">Post Comment</button>
                <button type="button" class="btn btn-secondary" id="Comments-Close">Close</button>
            </div>
        </div>
    </div>
</div>
