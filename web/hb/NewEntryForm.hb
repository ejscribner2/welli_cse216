<div id="NewEntryForm" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add a New Entry</h4>
                <button type="button" class="close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
							<label for="NewEntryForm-title">Location/Title</label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text" id="NewEntryForm-autoPrepend" style="display: none;">[REPOST]</span>
								</div>
								<input class="form-control" type="text" id="NewEntryForm-title" autofocus/>
							</div>
								<label for="NewEntryForm-message">Description</label>
								<textarea class="form-control" id="NewEntryForm-message"></textarea>
								<label for="NewEntryForm-breed">Breed</label>
								<input class="form-control" type="text" id="NewEntryForm-breed" autofocus/>



                <!--<div class="custom-file mt-3">-->
                    <!--<label class="custom-file-label" for="customFile">Choose file</label>-->
                    <!--<input type="file" class="custom-file-input" id="NewEntryForm-file">-->
                <!--</div>-->

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="NewEntryForm-OK">Post!</button>
                <button type="button" class="btn btn-secondary" id="NewEntryForm-Close">Close</button>
            </div>
        </div>
    </div>
</div>
