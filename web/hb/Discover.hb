<div id="Discover" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Discover</h4>
				<button type="button" class="close" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<div id="Discover-images" class="grid" data-masonry='{ "itemSelector": ".grid-item", "columnWidth": 152 }'>
					{{#each dogData.message}}
						<!--<div class="grid-item">-->
							<a href="#" class="img-clickable">
								<img class="grid-item" src={{this}} id="{{@key}}">
							</a>
						<!--</div>-->
					{{/each}}

				</div>

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" id="Discover-Close">Close</button>
			</div>
		</div>
	</div>
</div>
