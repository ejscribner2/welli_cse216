<div id="SeeAllComments" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">All Comments For Post {{mData.mId}}:</h4>
            </div>

            <div class="modal-body">
            <table class="table">
                <tbody>
                    {{#each mData.mComments}} 
                    <tr>
                        <td><button class="SeeAllComments-usersbtn" data-value="{{this.cSender}}">{{this.cSender}}</button></td>
                        <td>{{this.cComment}}</td>    
                    </tr>
                    {{/each}}
                </tbody>
            </table>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="SeeAllComments-Close">Close</button>
            </div>
        </div>
    </div>
</div>