<div id="TopBreeds" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<h4 class="modal-title">Top Breeds</h4>
				<button type="button" class="close" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<table class="table">
					<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Breed</th>
						<th scope="col">Post Count</th>
					</tr>
					</thead>
					<tbody>
					{{#each mData}}
						<tr class="{{assignLeaderPlace @key}}">
							<th scope="row">{{inc @key}}</th>
							<td>{{this.mBreed}}</td>
							<td>{{this.mNumPosts}}</td>
						</tr>
					{{/each}}
					</tbody>
				</table>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" id="TopBreeds-Close">Close</button>
			</div>

		</div>
	</div>
</div>
