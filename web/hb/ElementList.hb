<div class="container-fluid">
    <div id="ElementList">
        <div class="accordion" id="postAccordion">
					<div class="card" style="background: #bababa;">
						<div class="card-header row" id="" data-toggle="" data-placement="">
							<div class="col-2">
								Username
							</div>
							<div class="col-3">
								Title/Location
							</div>
							<div class="col">
								Body
							</div>
							<div class="col">
								Breed
							</div>

							<div class="col-auto">
								Upvotes
							</div>
							<div class="col-auto">
								Downvotes
							</div>
						</div>
					</div>
					<script>
						$(document).ready(function(){
							$('[data-toggle="tooltip"]').tooltip();
						});
					</script>
            {{#each mData}}
            <div class="card {{handleRepost this.mTitle}} label label-default" href="#" data-toggle="tooltip" data-placement="top" title="Click to View Comments">
                <div class="card-header row" id="{{this.mId}}" data-toggle="collapse" href="#collapse{{this.mId}}" data-value="{{this.mId}}" style="cursor: pointer;">
                    <div class="col-2 {{handleRepostText this.mTitle}}">
                        {{this.mUsername}}
                    </div>
                    <div class="col-3 {{handleRepostText this.mTitle}}">
                        {{this.mTitle}}
                    </div>
                    <div class="col {{handleRepostText this.mTitle}}">
                        {{this.mBody}}
                    </div>
										<div class="col {{handleRepostText this.mTitle}}">
											{{this.mBreed}}
										</div>

                    <div class="col-auto {{handleRepostText this.mTitle}}">
                        <button class="btn btn-secondary ElementList-likebtn" data-value="{{this.mId}}">
                            <i class="arrow up m-1"></i>
                        </button>
                        {{this.mUpvotes}}
                    </div>
                    <div class="col-auto {{handleRepostText this.mTitle}}">
                        <button class="btn btn-secondary ElementList-dislikebtn" data-value="{{this.mId}}">
                            <i class="arrow down m-1"></i>
                        </button>
                        {{this.mDownvotes}}
                    </div>
                </div>
                <div id="collapse{{this.mId}}" class="collapse commentCollapse" data-parent="#postAccordion" data-value="{{this.mId}}">
                    <div id="comments{{this.mId}}" class="card-body">
											<div class="row">
												<div class="col-md-9">
													<table id="commenttable{{this.mId}}" class="table table-bordered table-hover">
													</table>
													<button class="mb-0 ElementList-commentsbtn btn btn-success" data-value="{{this.mId}}">Add Comment</button>
												</div>
												<div class="col-md-3">
													<img src="{{this.mFile}}" class="item-img {{showRepostImg this.mTitle}}">
												</div>
											</div>
                    </div>
                </div>
            </div>
            {{/each}}
        </div>
    </div>
</div>
