class Profiles {

    private static readonly NAME = "Profiles";

    private static isInit = false;

    private static init() {
        if (!Profiles.isInit) {
            $("body").append(Handlebars.templates[Profiles.NAME + ".hb"]());
            Profiles.isInit = true;
        }
    }

    public static refresh() {
        Profiles.init();

        $.ajax({
            type: "GET",
            url: backendUrl + "/protected/users/" + ElementList.userProfileID + "?mUsername=",
            dataType: "json", 
            success: Profiles.update
        });
    }

    private static update(data: any) {
        $("#" + Profiles.NAME).remove();

        $("body").append(Handlebars.templates[Profiles.NAME + ".hb"](data));
        Profiles.show();

        $("#" + Profiles.NAME + "-Close").click(Profiles.hide);
    }

    private static hide() {
        $("#" + Profiles.NAME).modal("hide");
    }

    public static show() {
        $("#" + Profiles.NAME).modal("show");
    }


    
}
