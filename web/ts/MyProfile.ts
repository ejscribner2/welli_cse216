///<reference path="../app.ts"/>
class MyProfile {
    public static username:string = "";
    public static token:string = "";
    public static profileUrl:string = "";

    private static readonly NAME = "MyProfile";

    private static isInit = false;

    private static init() {
        if (!MyProfile.isInit) {
            $("body").append(Handlebars.templates[MyProfile.NAME + ".hb"]());
            MyProfile.isInit = true;
        }
    }

    public static refresh() {
        MyProfile.init();

        let userInfo:{[index: string] : any} = {};

        $.ajax({
            type: "GET",
            url: backendUrl + "/protected/users/" + MyProfile.username + "?mUsername=" + MyProfile.username + "&mToken=" + MyProfile.token,
            dataType: "json",
            success: (response: any) => {
                userInfo["profileInfo"] = response;
                $.ajax({
                    type: "GET",
                    url: backendUrl + "/protected/userPosts/" + MyProfile.username + "?mUsername=" + MyProfile.username + "&mToken=" + MyProfile.token,
                    dataType: "json",
                    success: (response: any) => {
                        userInfo["userPosts"] = response;
                        userInfo["profileUrl"] = MyProfile.profileUrl;
                        MyProfile.update(userInfo);
                    }
                });
            }
        });
    }

    private static update(data: any) {
        $("#" + MyProfile.NAME).remove();
    
        $("body").append(Handlebars.templates[MyProfile.NAME + ".hb"](data));
        MyProfile.show();

        $("#" + MyProfile.NAME + "-Close").click(MyProfile.hide);
        $("#" + MyProfile.NAME + "-editBio").click(MyProfile.clickedBio);
        $(".close").click(MyProfile.hide);
    }

    private static clickedBio() {
        let newBio = "" + $("#" + MyProfile.NAME + "-changeBio").val();
        if (newBio === "") {
            window.alert("Error: username or message is not valid");
            return;
        }
       // MyProfile.hide();
       var currentUser:string = $("#" + MyProfile.NAME + "");
            $.ajax({
                type: "PUT",
                url: backendUrl + "/protected/users/" +MyProfile.username +"/bio?mUsername=" + MyProfile.username + "&mBio=" + newBio,
                dataType: "json",
                success: MyProfile.onSubmitResponse
            });
    }

    private static hide() {
        $("#" + MyProfile.NAME + "-changeBio").val("");
        $("#" + MyProfile.NAME).modal("hide");
    }

    public static show() {
        $("#" + MyProfile.NAME + "-changeBio").val("");
        $("#" + MyProfile.NAME).modal("show");
    }


    /**
     * onSubmitResponse runs when the AJAX call in submitForm() returns a 
     * result.
     * 
     * @param data The object returned by the server
     */
    private static onSubmitResponse(data: any) {
        // If we get an "ok" message, clear the form and refresh the main 
        // listing of messages
        if (data.mStatus === "ok") {
            MyProfile.refresh();
            
        }
        // Handle explicit errors with a detailed popup message
        else if (data.mStatus === "error") {
            window.alert("The server replied with an error:\n" + data.mMessage);
        }
        // Handle other errors with a less-detailed popup message
        else {
            window.alert("Unspecified error");
        }
    }
}
