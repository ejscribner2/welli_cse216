class SeeAllComments {
    
    private static readonly NAME = "SeeAllComments";

    private static isInit = false;

public static refresh() {
    
    SeeAllComments.init();

    $.ajax({
        type: "GET",
        url: backendUrl + "/protected/posts/" + ElementList.commentID +"?mUsername=" + MyProfile.username + "&mToken=" + MyProfile.token,
        dataType: "json",
        success: SeeAllComments.update
    });

}

private static init() {
    if (!SeeAllComments.isInit) {
        SeeAllComments.isInit = true;
    }
}

private static update(data: any) {
    $("#" + SeeAllComments.NAME).remove();
    
    $("body").append(Handlebars.templates[SeeAllComments.NAME + ".hb"](data));

    SeeAllComments.show();
    $("#" + SeeAllComments.NAME + "-Close").click(SeeAllComments.hide);
    $("." + SeeAllComments.NAME + "-usersbtn").click(SeeAllComments.onUserProfile);
}

private static onUserProfile() {
    //added
    ElementList.userProfileID = $(this).data("value");
    SeeAllComments.hide();
    //changed
    Profiles.refresh();
}

private static hide() {
    $("#" + SeeAllComments.NAME).modal("hide");
}
public static show() {
    Comments.hide();
    $("#" + SeeAllComments.NAME).modal("show");
}

}
