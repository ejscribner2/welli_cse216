/**
 * The ElementList Singleton provides a way of displaying all of the data 
 * stored on the server as an HTML table.
 */
class ElementList {
    public static commentID: string = "";
    public static userProfileID: string = "";
    /**
     * The name of the DOM entry associated with ElementList
     */
    private static readonly NAME = "ElementList";

    /**
     * Track if the Singleton has been initialized
     */
    private static isInit = false;

    public static refresh() {
        // Make sure the singleton is initialized
        console.log("refreshing element list");

        ElementList.init();
        // Issue a GET, and then pass the result to update()
        $.ajax({
            type: "GET",
            url: backendUrl + "/protected/posts?mUsername=" + MyProfile.username + "&mToken=" + MyProfile.token,
            dataType: "json",
            success: (response: any) => {
                ElementList.update(response);
                console.log(response);
            }
        });
    }

    /**
     * Initialize the ElementList singleton.  
     * This needs to be called from any public static method, to ensure that the
     * Singleton is initialized before use.
     */
    private static init() {
        if (!ElementList.isInit) {
            Handlebars.registerHelper("handleRepost", function (title: string) {
                if(title.includes("[REPOST]")) {
                    return "border-warning repost-card";
                } else {
                    return "border-info original-card";
                }
            });
            Handlebars.registerHelper("handleRepostText", (title: string) => {
                if(title.includes("[REPOST]")) {
                    return "text-warning";
                } else {
                    return "text-info"
                }
            });
            Handlebars.registerHelper("showRepostImg", (title: string) => {
                if(title.includes("[REPOST]")) {
                    return "show";
                } else {
                    return "disappear";
                }
            });
            ElementList.isInit = true;
        }
    }


    /**
     * update() is the private method used by refresh() to update the 
     * ElementList
     */
    private static update(data: any) {
        console.log("updating element list");
        // Remove the table of data, if it exists
        $("#" + ElementList.NAME).remove();
        // Use a template to re-generate the table, and then insert it
        $("body").append(Handlebars.templates[ElementList.NAME + ".hb"](data));
        // Find all of the delete buttons, and set their behavior
        // $("." + ElementList.NAME + "-delbtn").click(ElementList.clickDelete);
        $("." + ElementList.NAME + "-likebtn").click(ElementList.clickLike);
        $("." + ElementList.NAME + "-dislikebtn").click(ElementList.clickDislike);
        $("." + ElementList.NAME + "-commentsbtn").click(ElementList.clickComment);
        $("." + ElementList.NAME + "-usersbtn").click(ElementList.clickUserProfile);
        $("." + ElementList.NAME + "-attachment").click(ElementList.viewAttachment);
        // Event handler for showing comments
        $(".commentCollapse").on("show.bs.collapse", (evt: any) => {
            ElementList.commentID = $(evt.target).data("value");
            let result:string = '<table class="table table-sm">';
            $.ajax({
                type: "GET",
                url: backendUrl + "/protected/posts/" + ElementList.commentID +"?mUsername=" + MyProfile.username + "&mToken=" + MyProfile.token,
                dataType: "json",
                success: (response: any) => {
                    result += "<thead>";
                    result += "<tr>";
                    result += `<th scope="col">User</th>`;
                    result += `<th scope="col">Comment</th>`;
                    result += "</tr>";
                    result += "</thead>";
                    for (let comment of response.mData.mComments) {
                        result += "<tr>";
                        result += `<th scope="row">${comment.cSender}</th>`;
                        result += `<td class="pl-4">${comment.cComment}</td>`;
                        result += "</tr>";
                    }
                    result += "</table>";
                    $(`#commenttable${ElementList.commentID}`).html(result);
                }
            });
        });
    }


    /*
    <thead>
														<tr>
															<th scope="col">User</th>
															<th scope="col">Comment</th>
														</tr>
													</thead>
     */

    /**
     * clickDelete is the code we run in response to a click of a delete button
     */
    // private static clickDelete() {
    //     // for now, just print the ID that goes along with the data in the row
    //     // whose "delete" button was clicked
    //     let id = $(this).data("value");
    //     $.ajax({
    //         type: "DELETE",
    //         url: backendUrl + "/messages/" + id,
    //         dataType: "json",
    //         success: ElementList.refresh
    //     });
    // }

    private static clickComment() {
        ElementList.commentID = $(this).data("value");
        Comments.show();
    }

    private static clickUserProfile() {
        ElementList.userProfileID = $(this).data("value");

        console.log("user profile is:" + ElementList.userProfileID);
        Profiles.refresh();
    }

    private static clickLike() {
        // for now, just print the ID that goes along with the data in the row
        // whose "delete" button was clicked
        let id = $(this).data("value");
        $.ajax({
            type: "PUT",
            url: backendUrl + "/protected/posts/upvotes/" + id + "?mUsername=" + MyProfile.username + "&mToken=" + MyProfile.token,
            dataType: "json",
            success: ElementList.refresh
        });
    }

    private static clickDislike() {

        let id = $(this).data("value");
        $.ajax({
            type: "PUT",
            url: backendUrl + "/protected/posts/downvotes/" + id + "?mUsername=" + MyProfile.username + "&mToken=" + MyProfile.token,
            dataType: "json",
            success: ElementList.refresh
        });
    }

    /**
     * Show a popup showing whatever file is associated with the post
     */
    private static viewAttachment() {
        let id = $(this).data("value"); /* Message id */
        // TODO: do a get request for the file and show it in a modal
    }
}
