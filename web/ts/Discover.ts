class Discover {

    private static isInit = false;
    private static readonly NAME = "Discover";

    public static dogUrls: string[];

    private static init() {
        if(!Discover.isInit) {
            $("body").append(Handlebars.templates[Discover.NAME+".hb"]());

            this.isInit = true;
        }
    }

    public static refresh() {
        Discover.init();
        let dogData:{[index: string] : any} = {};

        $.ajax({
            type: "GET",
            url: "https://dog.ceo/api/breeds/image/random/12",
            dataType: "json",
            success: (response: any) => {
                dogData["dogData"] = response;
                console.log(dogData["dogData"].message);
                this.dogUrls = dogData["dogData"].message;
                Discover.update(dogData);
            }
        });

        Discover.handleImageClicked();
    }

    private static handleImageClicked() {
        $(document).on("click", "#"+Discover.NAME+"-images", (e: any) => {
            e.preventDefault();
            console.log(e.target.id);
            console.log(e.target.src);
            Discover.hide(); // check about position of this
            NewEntryForm.createRepost(e.target.src);
        });
    }

    private static update(data: any) {
        $("#" + Discover.NAME).remove();

        $("body").append(Handlebars.templates[Discover.NAME + ".hb"](data));
        Discover.show();

        $("#" + Discover.NAME + "-Close").click(Discover.hide);
        $(".close").click(Discover.hide);
    }

    private static hide() {
        $("#" + Discover.NAME + "-images").html("");
        $("#" + Discover.NAME).modal("hide");
    }

    public static show() {
        $("#" + Discover.NAME).modal("show");
    }
}
