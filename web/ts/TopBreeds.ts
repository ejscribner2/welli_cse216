class TopBreeds {

    private static isInit = false;
    private static readonly NAME = "TopBreeds";


    private static init() {
        if(!TopBreeds.isInit) {
            $("body").append(Handlebars.templates[TopBreeds.NAME+".hb"]());
            Handlebars.registerHelper("inc", function(value: any)
            {
                return parseInt(value) + 1;
            });

            Handlebars.registerHelper("assignLeaderPlace", function(value: any)
            {
                let place: number = parseInt(value) + 1;
                if(place === 1) {
                    return "table-success";
                }
                if(place === 2) {
                    return "table-primary";
                }
                if(place === 3) {
                    return "table-warning";
                }
            });

            this.isInit = true;
        }
    }

    public static refresh() {
        TopBreeds.init();

        $.ajax({
            type: "GET",
            url: backendUrl + "/protected/users/topbreeds?mUsername=" + MyProfile.username + "&mToken=" + MyProfile.token,
            dataType: "json",
            success: (response: any) => {
                console.log("response is: ");
                console.log(response);
                console.log(response.type);
                TopBreeds.update(response);
            }
        });
    }



    private static update(data: any) {
        $("#" + TopBreeds.NAME).remove();
        $("body").append(Handlebars.templates[TopBreeds.NAME + ".hb"](data));

        TopBreeds.show();
        $("#" + TopBreeds.NAME + "-Close").click(TopBreeds.hide);
        $(".close").click(TopBreeds.hide);
    }

    private static hide() {
        $("#" + TopBreeds.NAME + "-images").html("");
        $("#" + TopBreeds.NAME).modal("hide");
    }

    public static show() {
        $("#" + TopBreeds.NAME).modal("show");
    }
}
