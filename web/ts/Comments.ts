class Comments {

    private static readonly NAME = "Comments";

    private static isInit = false;

    
    private static init() {
        if (!Comments.isInit) {
            $("body").append(Handlebars.templates[Comments.NAME + ".hb"]());
            $("#" + Comments.NAME + "-OK").click(Comments.submitForm);
            $("#" + Comments.NAME + "-Close").click(Comments.hide);
            $("#" + Comments.NAME + "-SeeAll").click(SeeAllComments.refresh);
           
            Comments.isInit = true;
        }
    }

    public static refresh() {
        Comments.init();
    }

    public static hide() {
        $("#" + Comments.NAME + "-message").val("");
        $("#" + Comments.NAME).modal("hide");
    }

    public static show() {
        $("#" + Comments.NAME + "-message").val("");
        $("#" + Comments.NAME).modal("show");
    }


    /**
     * Send data to submit the form only if the fields are both valid.  
     * Immediately hide the form when we send data, so that the user knows that 
     * their click was received.
     */
    private static submitForm() {
        // get the values of the two fields, force them to be strings, and check 
        // that neither is empty
        //let username = "" + $("#" + Comments.NAME + "-username").val();
        let msg = "" + $("#" + Comments.NAME + "-message").val();
        if (msg === "") {
            window.alert("Error: enter a message");
            return;
        }
        Comments.hide();
       
        $.post(
            backendUrl + "/protected/posts/" + ElementList.commentID + "/comment", 
            {
                mUsername: MyProfile.username,
                mToken: MyProfile.token,
                cBody: msg
            },
            Comments.onSubmitResponse,
            'json'
        ).then(
            $(`#collapse${ElementList.commentID}`).collapse('toggle')
        );
    }

    /**
     * onSubmitResponse runs when the AJAX call in submitForm() returns a 
     * result.
     * 
     * @param data The object returned by the server
     */
    private static onSubmitResponse(data: any) {
        // If we get an "ok" message, clear the form and refresh the main 
        // listing of messages
        if (data.mStatus === "ok") {
            Comments.refresh();
        }
        // Handle explicit errors with a detailed popup message
        else if (data.mStatus === "error") {
            window.alert("The server replied with an error:\n" + data.mMessage);
        }
        // Handle other errors with a less-detailed popup message
        else {
            window.alert("Unspecified error");
        }
    }
}
