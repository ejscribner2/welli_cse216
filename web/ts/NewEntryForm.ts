/**
 * NewEntryForm encapsulates all of the code for the form for adding an entry
 */
class NewEntryForm {

    /**
     * The name of the DOM entry associated with NewEntryForm
     */
    private static readonly NAME = "NewEntryForm";


    private static photoUrl: string;

    /**
     * Track if the Singleton has been initialized
     */
    private static isInit = false;
    static file: File;

    static prefix: string = "[REPOST]";

    /**
     * Initialize the NewEntryForm by creating its element in the DOM and 
     * configuring its buttons.  This needs to be called from any public static 
     * method, to ensure that the Singleton is initialized before use
     */
    private static init() {
        if (!NewEntryForm.isInit) {
            $("#" + NewEntryForm.NAME + "-autoPrepend").css("display", "none");
            $("#" + NewEntryForm.NAME + "-breed").prop("readonly", false);
            $("body").append(Handlebars.templates[NewEntryForm.NAME + ".hb"]());
            $("#" + NewEntryForm.NAME + "-OK").click(NewEntryForm.submitForm);
            $("#" + NewEntryForm.NAME + "-Close").click(NewEntryForm.hide);
            $(".close").click(NewEntryForm.hide);
            $("#" + NewEntryForm.NAME + "-file").bind("change", (evt: any) => {
                this.file = evt.target.files[0];
            });
            NewEntryForm.isInit = true;
        }
    }

    /**
     * Refresh() doesn't really have much meaning, but just like in sNavbar, we
     * have a refresh() method so that we don't have front-end code calling
     * init().
     */
    public static refresh() {
        NewEntryForm.init();
    }

    /**
     * Hide the NewEntryForm.  Be sure to clear its fields first
     */
    private static hide() {
        $("#" + NewEntryForm.NAME + "-autoPrepend").hide();
        $("#" + NewEntryForm.NAME + "-title").val("");
        $("#" + NewEntryForm.NAME + "-message").val("");
        $("#" + NewEntryForm.NAME + "-breed").val("").prop("readonly", false);
        $("#" + NewEntryForm.NAME).modal("hide");
    }

    public static createRepost(url: string) {
        NewEntryForm.photoUrl = url;
        let breed: string = this.filterBreed(url);
        $("#" + NewEntryForm.NAME + "-title").val("");
        $("#" + NewEntryForm.NAME + "-autoPrepend").show();
        $("#" + NewEntryForm.NAME + "-message").val("");
        $("#" + NewEntryForm.NAME + "-breed").val(breed).prop("readonly", true);
        $("#" + NewEntryForm.NAME).modal("show");
    }

    private static filterBreed(url: string) {
        let lastSlash = url.lastIndexOf("/");
        return url.substring(30, lastSlash);
    }

    // /**
    //  * Show the NewEntryForm.  Be sure to clear its fields, because there are
    //  * ways of making a Bootstrap modal disapper without clicking Close, and
    //  * we haven't set up the hooks to clear the fields on the events associated
    //  * with those ways of making the modal disappear.
    //  */
    // public static show() {
    //     $("#" + NewEntryForm.NAME + "-title").val("");
    //     $("#" + NewEntryForm.NAME + "-message").val("");
    //     $("#" + NewEntryForm.NAME + "-file").val("");
    //     $("#" + NewEntryForm.NAME).modal("show");
    // }f


    /**
     * Send data to submit the form only if the fields are both valid.  
     * Immediately hide the form when we send data, so that the user knows that 
     * their click was received.
     */
    private static submitForm() {
        // get the values of the two fields, force them to be strings, and check 
        // that neither is empty

        // if is repost, prepend with prefix (can be localized) and show the pane, otherwise hide it
        let title;
        if($("#" + NewEntryForm.NAME + "-autoPrepend").is(":visible")) { // is a repost
            title = "[REPOST] - " + $("#" + NewEntryForm.NAME + "-title").val();
        } else {
            title = "" + $("#" + NewEntryForm.NAME + "-title").val();
        }

        let msg = "" + $("#" + NewEntryForm.NAME + "-message").val();
        let breed = "" + $("#" + NewEntryForm.NAME + "-breed").val();
        breed.toLowerCase();
        // let encoded = "";
        if (title === "" || msg === "") {
            window.alert("Error: username or message is not valid");
            return;
        }
        NewEntryForm.hide();

        let encoded: any;
        // Serialize file and send it to the backend
        if (this.file != null) {
            var reader = new FileReader();
            reader.readAsBinaryString(this.file);
            reader.onload = (e) => {
                // @ts-ignore
                // TODO: add a null check!
                encoded = window.btoa(<string>e.target.result);
            };
        }

        $.post(
            backendUrl + "/protected/posts", 
            {
                mUsername: MyProfile.username,
                mToken: MyProfile.token,
                mTitle: title,
                mBody: msg,
                mBreed: breed,
                mFile: NewEntryForm.photoUrl
            },
            NewEntryForm.onSubmitResponse)
    }

    /**
     * onSubmitResponse runs when the AJAX call in submitForm() returns a 
     * result.
     * 
     * @param data The object returned by the server
     */
    private static onSubmitResponse(data: any) {
        // If we get an "ok" message, clear the form and refresh the main 
        // listing of messages
        if (data.mStatus === "ok") {
            ElementList.refresh();
        }
        // Handle explicit errors with a detailed popup message
        else if (data.mStatus === "error") {
            window.alert("The server replied with an error:\n" + data.mMessage);
        }
        // Handle other errors with a less-detailed popup message
        else {
            window.alert("Unspecified error");
        }
    }
}
