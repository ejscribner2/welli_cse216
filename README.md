# CSE216 Project - Team WELLI

Updated Oct 22 2019 (@End of Phase 2) by Elliot Scribner

**Team Members**:

- Lydia Cornwell
- Isabel Dewalt
- Walker Finlay
- Elliot Scribner
- Lilly Zhu 

## Development Environment Setup

- Clone the git repository from bitbucket

        git clone https://ejscribner2@bitbucket.org/ejscribner2/welli_cse216.git

    *Note:* the `master` branch has been set up with preliminary backend code which has been deployed to heroku at:

        https://hidden-sierra-61431.herokuapp.com/

    

- The heroku app is attached to a database with the schema detailed in the `welli_schema2.0.sql` file
    
- The `/protected/posts` route exposes all posts from the database, and the `/protected/posts/:id` route exposes individual posts. 
Both routes fit the data specification: 

        {
          mStatus,
          mData: [
            {
              mId,
              mUsername,
              mTitle,
              mBody,
              mUpvotes,
              mDownvotes,
              mComments: [
                {
                  cId,
                  cSender,
                  cComment,
                  cDate
                }
              ]
            }
          ]
        }    
        
    *Note:* the `mComments` field is null for the get all route.

- An `index.html` page is served from `/src/main/resources` and should be replaced by the index files deployed to that 
directory by a deploy script (web developer). Although it is currently checked in to git just to show where to serve 
content from, it should be added to `backend/.gitignore` and removed from the maven repository in the future.
- *Note to Web Developer:* In order for front end code to be deployed to heroku, the current setup requires a full 
`mvn package; mvn heroku:deploy` from the backend. In order to avoid issues with backend it might make sense to either 
develop front end code locally or deploy to a seperate heroku project for testing. A tutorial on setting up CORS can be found [here](http://www.cse.lehigh.edu/~spear/cse216_tutorials/tut_cors/index.html).


## A Note on Using Git as a Team

At the beginning of each phase, each role will create their own branch where they can work within their roles directory. 
Additionally, I have made the master branch protected such that all code is only merged in via pull request that way it 
can be reviewed by everyone else on the team (esp. project manager).

### How to create a pull request
A guide to creating a pull request is available [here.](https://www.atlassian.com/git/tutorials/making-a-pull-request)

The basic concept is:

- Everyone makes all of their changes to their branches
- Those changes get pushed to the remote (bitbucket website)
- Before integrating those changes into the master, they need to be reviewed by team members
- The developer who pushed the changes opens a new pull request requesting to merge their branch with their changes into the master
- This pull request is then reviewed by other team members and can be commented on, approved, or have changes requested.
- Once the pull request is approved, it can be merged into the master
