package edu.lehigh.cse216.welli;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.ViewHolder> {

    interface ClickListener{
        void onClick(PostData d);
        void onUpvoteClick(int position, PostData postId);
        void onDownvoteClick(int position, PostData postId);
    }
    private ClickListener mClickListener;
    ClickListener getClickListener() {return mClickListener;}
    void setClickListener(ClickListener c) { mClickListener = c;}


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mUsername; // should be button?
        TextView mTitle;
        TextView mBody;
        TextView mUpvotes;
        TextView mDownvotes;
        ImageView mUpvoteBtn;
        ImageView mDownvoteBtn;

        ViewHolder(View itemView,final ClickListener listener, final ArrayList<PostData> data) {
            super(itemView);
            this.mUsername = (TextView) itemView.findViewById(R.id.listItemPoster);
            this.mTitle = (TextView) itemView.findViewById(R.id.listItemTitle);
            this.mBody = (TextView) itemView.findViewById(R.id.listItemBody);
            this.mUpvotes = (TextView) itemView.findViewById(R.id.listItemUpvotes);
            this.mDownvotes = (TextView) itemView.findViewById(R.id.listItemDownvotes);
            this.mUpvoteBtn = itemView.findViewById(R.id.image_like);
            this.mDownvoteBtn = itemView.findViewById(R.id.image_dislike);

            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PostData datum = data.get(getAdapterPosition());

                    listener.onClick(datum);

                }
            });

            this.mUpvoteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("Response", "Hello it kind of worked***");
                    //Log.d("listener", (listener.toString()));
                    if (listener != null) {
                        Log.d("Response", "Hello it kind of worked***222");
                        int position = getAdapterPosition();
                        PostData datum = data.get(getAdapterPosition());
                        if (position != RecyclerView.NO_POSITION) {
                            Log.d("Response", "Hello it kind of worked***111");
                            listener.onUpvoteClick(position, datum);
                        }
                    }
                }
            });

            this.mDownvoteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("Response", "Hello it kind of worked***");
                    //Log.d("listener", (listener.toString()));
                    if (listener != null) {
                        Log.d("Response", "Hello it kind of worked***222");
                        int position = getAdapterPosition();
                        PostData datum = data.get(getAdapterPosition());
                        if (position != RecyclerView.NO_POSITION) {
                            Log.d("Response", "Hello it kind of worked***111");
                            listener.onDownvoteClick(position, datum);
                        }
                    }
                }
            });


        }
    }

    private ArrayList<PostData> mData;
    private LayoutInflater mLayoutInflater;

    ItemListAdapter(Context context, ArrayList<PostData> data, ClickListener listener) {
        mData = data;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mClickListener = listener;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.list_item, null);
        return new ViewHolder(view, mClickListener, mData);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final PostData d = mData.get(position);
        holder.mUsername.setText(d.mUsername);
        holder.mTitle.setText(d.mTitle);
        holder.mBody.setText(d.mBody);
        holder.mUpvotes.setText(Integer.toString(d.mUpvotes));
        holder.mDownvotes.setText(Integer.toString(d.mDownvotes));

        // Attach a click listener to the view we are configuring
        final View.OnClickListener listener = new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                mClickListener.onClick(d);
            }
        };
        holder.mUsername.setOnClickListener(listener);
        holder.mTitle.setOnClickListener(listener);
        holder.mBody.setOnClickListener(listener);
        holder.mUpvotes.setOnClickListener(listener);
        holder.mDownvotes.setOnClickListener(listener);

    }
}