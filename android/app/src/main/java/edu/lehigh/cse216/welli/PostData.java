package edu.lehigh.cse216.welli;

import java.util.ArrayList;

/**
 * PostData is like a struct in C: we use it to hold data, and we allow
 * direct access to its fields.  In the context of this Database, PostData
 * represents the data we'd see in a row.
 * <p>
 * We make PostData a static class of Database because we don't really want
 * to encourage users to think of PostData as being anything other than an
 * abstract representation of a row of the database.  PostData and the
 * Database are tightly coupled: if one changes, the other should too.
 */
public class PostData {
    /**
     * The ID of this post
     */
    int mId;
    /**
     * The sender of the post
     */
    String mUsername;
    /**
     * The title of the post
     */
    String mTitle;
    /**
     * The body of the post stored in this row
     */
    String mBody;
    /**
     * The date it was posted
     */
    String mDate;
    /**
     * The number of upvotes for this post
     */
    int mUpvotes;
    /**
     * The number of downvotes for this post
     */
    int mDownvotes;
    /**
     * The comments associated with the post
     */
    ArrayList<CommentData> mComments;

    /**
     * Construct a PostData object by providing values for its fields
     */
    PostData(int id, String username, String title, String body, String date, int upvotes, int downvotes, ArrayList<CommentData> comments) {
        mId = id;
        mUsername = username;
        mTitle = title;
        mBody = body;
        mDate = date;
        mUpvotes = upvotes;
        mDownvotes = downvotes;
        mComments = comments;
    }
}
