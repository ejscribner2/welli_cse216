package edu.lehigh.cse216.welli;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "MainActivity";
    private static final int RC_GET_TOKEN = 9002;
    private static final int RC_ADD_POST = 9003;
    private static final int RC_VIEW_PROFILE = 9004;
    private GoogleSignInClient mGoogleSignInClient;

    /**
     * mData holds the data we get from Volley
     */
    ArrayList<PostData> mData = new ArrayList<>();

    String mUsername = null;
    private static String mToken = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // should try to auth w/ remembered info
        // otherwise display msg with "login" info
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        // Button click listeners
        findViewById(R.id.sign_in_button).setOnClickListener(this);
        findViewById(R.id.action_settings_create).setOnClickListener(this);

        // For sample only: make sure there is a valid server client ID.
        validateServerClientID();

        // [START configure_signin]
        // Request only the user's ID token, which can be used to identify the
        // user securely to your backend. This will contain the user's basic
        // profile (name, profile picture URL, etc) so you should not need to
        // make an additional call to personalize your application.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build();
        // [END configure_signin]

        // Build GoogleAPIClient with the Google Sign-In API and the above options.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        Log.d("icd221", "Debug Message from onCreate");
    }

    private void getIdToken() {
        // Show an account picker to let the user choose a Google account from the device.
        // If the GoogleSignInOptions only asks for IDToken and/or profile and/or email then no
        // consent screen will be shown here.
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_GET_TOKEN);
    }

    private void refreshIdToken() {
        // Attempt to silently refresh the GoogleSignInAccount. If the GoogleSignInAccount
        // already has a valid token this method may complete immediately.
        //
        // If the user has not previously signed in on this device or the sign-in has expired,
        // this asynchronous branch will attempt to sign in the user silently and get a valid
        // ID token. Cross-device single sign on will occur in this branch.
        mGoogleSignInClient.silentSignIn()
                .addOnCompleteListener(this, new OnCompleteListener<GoogleSignInAccount>() {
                    @Override
                    public void onComplete(@NonNull Task<GoogleSignInAccount> task) {
                        handleSignInResult(task);
                    }
                });
    }
    private void getPosts() {
        mData.clear();
        findViewById(R.id.post_list_view).setVisibility(View.VISIBLE);
        String postsUrl = "https://hidden-sierra-61431.herokuapp.com/protected/posts?mUsername=" + mUsername + "&mToken=" + mToken;
//        String postsUrl = "http://infinite-island-04530.herokuapp.com/posts.json"; // mocked until auth works
        StringRequest getAllPostsReq = new StringRequest(Request.Method.GET, postsUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("resp is", response);
                        populateListFromVolley(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "That didn't work! " + error);
                    }
                }
        ) {

        };


        // Add the request to the RequestQueue.
        VolleySingleton.getInstance(this).addToRequestQueue(getAllPostsReq);
    }

    private void populateListFromVolley(final String response){
        try {
            //Convert the json object to an array
            JSONObject obj = new JSONObject(response);
            JSONArray json  = obj.getJSONArray("mData"); // should this be mData??

            for (int i = 0; i < json.length(); ++i) {
                int mId = json.getJSONObject(i).getInt("mId");
                String mUsername = json.getJSONObject(i).getString("mUsername");
                String mTitle = json.getJSONObject(i).getString("mTitle");
                String mBody = json.getJSONObject(i).getString("mBody");
                String mDate = json.getJSONObject(i).getString("mDate");
                int mUpvotes = json.getJSONObject(i).getInt("mUpvotes");
                int mDownvotes = json.getJSONObject(i).getInt("mDownvotes");
                //mData.add(new Post(mUsername, mMessage));
                mData.add(new PostData(mId, mUsername, mTitle, mBody, mDate, mUpvotes, mDownvotes, null));
                //Figure out how to have it display the username too
            }
        } catch (final JSONException e) {
            Log.d("icd221", "Error parsing JSON file: " + e.getMessage());
            return;
        }
        Log.d("icd221", "Successfully parsed JSON file.");

        RecyclerView rv = (RecyclerView) findViewById(R.id.post_list_view);
        rv.setLayoutManager(new LinearLayoutManager(this));


        final RequestQueue queue = Volley.newRequestQueue(this);

        ItemListAdapter.ClickListener listener = new ItemListAdapter.ClickListener() {
            @Override
            public void onClick(PostData d) {
                // this should probably be deleted
                Toast.makeText(MainActivity.this, d.mUsername + " --> " + d.mTitle, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onUpvoteClick(int position, PostData d){
                //
                Log.d("Response", "It clicked!!!!!!!!!!!!!!!");


                String url = "https://hidden-sierra-61431.herokuapp.com/protected/posts/upvotes/" + d.mId + "?mUsername=" + mUsername + "&mToken=" + mToken;

                StringRequest likeRequest = new StringRequest(Request.Method.PUT, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                // response
                                Log.d("Response", response);
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
//                                Log.d("Error.Response", response);
                                Log.d("Error.Response", "OOPS FIX THIS");
                            }
                        }
                ) {

                };

                queue.add(likeRequest);
                getPosts();
            }

            public void onDownvoteClick(int position, PostData d){
                //
                Log.d("Response", "It clicked!!!!!!!!!!!!!!!");


                String url = "https://hidden-sierra-61431.herokuapp.com/protected/posts/downvotes/" + d.mId + "?mUsername=" + mUsername + "&mToken=" + mToken;

                StringRequest likeRequest = new StringRequest(Request.Method.PUT, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                // response
                                Log.d("Response", response);
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
//                                Log.d("Error.Response", response);
                                Log.d("Error.Response", "OOPS FIX THIS");
                            }
                        }
                ) {

                };

                queue.add(likeRequest);
                getPosts();
            }
        };

        ItemListAdapter adapter = new ItemListAdapter(this, mData, listener);
        rv.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    // [START handle_sign_in_result]
    private void handleSignInResult(@NonNull Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            mToken = account.getIdToken();
            mUsername = account.getEmail();
            if(mToken != null && mUsername != null) {
                updateUI(account);
            } else {
                updateUI(null);
            }
            // TODO(developer): send ID Token to server and validate
            Log.e("ID TOKEN IS!!!!", mToken);
        } catch (ApiException e) {
            Log.w(TAG, "handleSignInResult:error", e);
            updateUI(null);
        }
    }
    // [END handle_sign_in_result]

    private void signOut() {
        mGoogleSignInClient.signOut().addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                updateUI(null);
            }
        });
    }

    private void updateUI(@Nullable GoogleSignInAccount account) {
        if (account != null) {
            sendLoginRequest();
            findViewById(R.id.sign_in_button).setVisibility(View.GONE);
            findViewById(R.id.create_new_post).setVisibility(View.VISIBLE);
            getPosts();
        } else {
            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
            findViewById(R.id.create_new_post).setVisibility(View.GONE);
            findViewById(R.id.post_list_view).setVisibility(View.GONE);
        }
    }

    private void validateServerClientID() {
        String serverClientId = getString(R.string.server_client_id);
        String suffix = ".apps.googleusercontent.com";
        if (!serverClientId.trim().endsWith(suffix)) {
            String message = "Invalid server client ID in strings.xml, must end with " + suffix;

            Log.w(TAG, message);
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        }
    }

    public void sendLoginRequest() { // here -> need to be sending a "login" req firstS
        String url = "https://hidden-sierra-61431.herokuapp.com/login?mToken=" + mToken;
        StringRequest loginRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
//                                Log.d("Error.Response", response);
                        Log.d("Error.Response", "OOPS FIX THIS");
                    }
                }
        ) {

        };
        Log.e(TAG, "Logging in right now");
        VolleySingleton.getInstance(this).addToRequestQueue(loginRequest);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                getIdToken();
                break;
            case R.id.action_settings_create:
                Intent i = new Intent(getApplicationContext(), AddPostActivity.class);
                i.putExtra("label_contents", "What's going on?");
                i.putExtra("mToken", mToken);
                i.putExtra("mUsername", mUsername);
                startActivityForResult(i, RC_ADD_POST); // 789 is the number that will come cardBack to us
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.sign_out_button) {
            signOut();
        }

        if (id == R.id.view_profile_button) {
            Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
            i.putExtra("mToken", mToken);
            i.putExtra("mUsername", mUsername);
            startActivityForResult(i, RC_VIEW_PROFILE); // 789 is the number that will come cardBack to us
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) { // this watches/listsn for changes??
        getPosts();
        // Check which request we're responding to
        if (requestCode == RC_ADD_POST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // Get the "extra" string of data
                Toast.makeText(MainActivity.this, data.getStringExtra("result"), Toast.LENGTH_LONG).show();
            }
        }

        if (requestCode == RC_VIEW_PROFILE) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(MainActivity.this, "hello from the profile", Toast.LENGTH_LONG).show();
            }
        }

        if (requestCode == RC_GET_TOKEN) {
            // [START get_id_token]
            // This task is always completed immediately, there is no need to attach an
            // asynchronous listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
            // [END get_id_token]
        }

    }
}
