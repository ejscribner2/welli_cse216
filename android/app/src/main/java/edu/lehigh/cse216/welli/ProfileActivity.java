package edu.lehigh.cse216.welli;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import org.json.JSONException;
import org.json.JSONObject;



public class ProfileActivity extends AppCompatActivity {
    String token;
    String username;
    private static final String TAG = "ProfileActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        findViewById(R.id.view_profile).setVisibility(View.VISIBLE);

        final Intent profileInfo = getIntent();
        token = profileInfo.getStringExtra("mToken");
        username = profileInfo.getStringExtra("mUsername");

        Button updateBioBtn = (Button) findViewById(R.id.buttonUpdateBio);
        Button returnBtn = (Button) findViewById(R.id.buttonReturn);

        updateBioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG,"update bio!");
                // this code should make a request to update the bio
                // ...
            }
        });

        returnBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG, "return");
                // this code should return to main activity
                // ...
                setResult(Activity.RESULT_OK); // use result_canceled?
                finish();
            }
        });



        getProfile(username);

    }

    private void updateUI(ProfileData mData) {
        TextView name = (TextView)findViewById(R.id.profileName);
        TextView username = (TextView)findViewById(R.id.profileUsername);
        TextView email = (TextView)findViewById(R.id.profileEmail);
        TextView bio = (TextView)findViewById(R.id.profileBio);

        if (mData != null) {
            String fullName = mData.mFirstname + " " + mData.mLastname;
            name.setText(fullName);
            username.setText(mData.mUsername);
            email.setText(mData.mEmail);
            bio.setText(mData.mBio);
        }
    }

    private void getProfile(String usernameToGet) {
//        String profUrl = "http://infinite-island-04530.herokuapp.com/profs.json"; // mocked until auth works
        String profUrl = "https://hidden-sierra-61431.herokuapp.com/protected/users/" + usernameToGet + "?mUsername=" + username + "&mToken=" + token;
        StringRequest getProfileReq = new StringRequest(Request.Method.GET, profUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ProfileData mData = populateProfile(response);
                updateUI(mData);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "That didn't work!");
                    }
                }
        ) {

        };


        VolleySingleton.getInstance(this).addToRequestQueue(getProfileReq);
    }

    private ProfileData populateProfile(final String response) {
        try {
            //Convert the json object to an array
            JSONObject obj = new JSONObject(response);

            String mFirstname = obj.getJSONObject("mData").getString("mFirstname");
            String mLastname = obj.getJSONObject("mData").getString("mLastname");
            String mUsername = obj.getJSONObject("mData").getString("mUsername");
            String mEmail = obj.getJSONObject("mData").getString("mEmail");
            String mBio = obj.getJSONObject("mData").getString("mBio");
            return new ProfileData(mFirstname, mLastname, mUsername, mEmail, mBio);
        } catch (final JSONException e) {
            Log.d(TAG, "Error parsing JSON file: " + e.getMessage());
            return null;
        }
    }
}