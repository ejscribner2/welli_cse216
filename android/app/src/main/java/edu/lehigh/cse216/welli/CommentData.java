package edu.lehigh.cse216.welli;

/**
 * CommentData acts as a data structure to hold all data associated with a
 * comment object
 */
public class CommentData {
    /**
     * The ID of this comment
     */
    int cId;
    /**
     * The sender of this comment
     */
    String cSender;
    /**
     * The comment's contents
     */
    String cComment;
    /**
     * The date it was commented
     */
    String cDate;

    /**
     * Construct a CommentData with the:
     *
     * @param id      of comment
     * @param sender  of comment
     * @param comment contents
     * @param date    comment sent
     */
    CommentData(int id, String sender, String comment, String date) {
        cId = id;
        cSender = sender;
        cComment = comment;
        cDate = date;
    }
}
