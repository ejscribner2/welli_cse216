package edu.lehigh.cse216.welli;

/**
 * CommentData acts as a data structure to hold all data associated with a
 * comment object
 */
public class ProfileData {
    /**
     * The users first name
     */
    String mFirstname;
    /**
     * The users last name
     */
    String mLastname;
    /**
     * The username
     */
    String mUsername;
    /**
     * The email of the user
     */
    String mEmail;
    /**
     * The bio of the user
     */
    String mBio;


    ProfileData(String firstName, String lastName, String username, String email, String bio) {
        mFirstname = firstName;
        mLastname = lastName;
        mUsername = username;
        mEmail = email;
        mBio = bio;
    }
}
