# User Interface Documentation
**Updated:** 9/23/2019

This document details the general flow throughout the application, and how users should be presented with data as they
create, send, and like messages.

## Pages

1. Home
    - Header
        - Logo/Team name
        - Add Message Button
    - List
        - Displays:
            
                Username, Message, Number of Likes, Like Button
                
        - Scrolls through list of messages, should show separation between messages
        
2. Add Message
    - Header
        - (Should be same as home or similar with `add message` context)
    - Field for username (of poster)
    - Field for message (to post)
    - Button to cancel post
    - Button to submit (post) post
 
