drop table downvotes, upvotes, comment, post, chat_users;

CREATE TABLE chat_users
(
  u_id      SERIAL PRIMARY KEY,
  username  VARCHAR(255),
  firstname VARCHAR(255),
  lastname  VARCHAR(255),
  email     VARCHAR(255),
  bio       VARCHAR(555),
  salt      VARCHAR(255),
  passHash  VARCHAR(255)
);


CREATE TABLE post
(
  p_id      SERIAL PRIMARY KEY,
  sender_id INTEGER      NOT NULL,
  title     VARCHAR(50)  NOT NULL,
  body      VARCHAR(140) NOT NULL,
  breed      VARCHAR(50) NOT NULL,
  date      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (sender_id) REFERENCES chat_users (u_id)
    ON DELETE CASCADE
);

CREATE TABLE comment
(
  c_id      SERIAL PRIMARY KEY,
  post_id   INTEGER NOT NULL,
  sender_id INTEGER NOT NULL,
  body      VARCHAR(140),
  date      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (sender_id) REFERENCES chat_users (u_id)
    ON DELETE CASCADE,
  FOREIGN KEY (post_id) REFERENCES post (p_id)
);

CREATE TABLE upvotes
(
  u_id INTEGER NOT NULL,
  p_id INTEGER NOT NULL,
  FOREIGN KEY (p_id) REFERENCES post (p_id)
    ON DELETE CASCADE,
  FOREIGN KEY (u_id) REFERENCES chat_users (u_id)
    ON DELETE CASCADE,
  PRIMARY KEY (u_id, p_id)
);

CREATE TABLE downvotes
(
  u_id INTEGER NOT NULL,
  p_id INTEGER NOT NULL,
  FOREIGN KEY (p_id) REFERENCES post (p_id)
    ON DELETE CASCADE,
  FOREIGN KEY (u_id) REFERENCES chat_users (u_id)
    ON DELETE CASCADE,
  PRIMARY KEY (u_id, p_id)
);

--These procedures/triggers handle keeping the votes table sane
CREATE OR REPLACE FUNCTION handle_downvotes() RETURNS trigger AS
$handle_downvotes$
DECLARE
  dv_row downvotes%rowtype;
BEGIN
  FOR dv_row IN
    SELECT * from downvotes
    LOOP
      IF dv_row.u_id = NEW.u_id AND dv_row.p_id = NEW.p_id
      THEN
        DELETE
        FROM downvotes
        WHERE downvotes.u_id = dv_row.u_id;
      end if;
    end loop;
    RETURN new;
end;
$handle_downvotes$ LANGUAGE plpgsql;

CREATE TRIGGER upvotes_handler
  BEFORE INSERT OR UPDATE ON upvotes
  FOR EACH ROW
  EXECUTE PROCEDURE handle_downvotes();


CREATE OR REPLACE FUNCTION handle_upvotes() RETURNS trigger AS
$handle_downvotes$
DECLARE
  uv_row upvotes%rowtype;
BEGIN
  FOR uv_row IN
    SELECT * from upvotes
    LOOP
      IF uv_row.u_id = NEW.u_id AND uv_row.p_id = NEW.p_id
      THEN
        DELETE
        FROM upvotes
        WHERE upvotes.u_id = uv_row.u_id;
      end if;
    end loop;
    RETURN new;
end;
$handle_downvotes$ LANGUAGE plpgsql;

CREATE TRIGGER downvotes_handler
  BEFORE INSERT OR UPDATE ON downvotes
  FOR EACH ROW
  EXECUTE PROCEDURE handle_upvotes();

