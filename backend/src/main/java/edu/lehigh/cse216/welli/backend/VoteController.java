package edu.lehigh.cse216.welli.backend;

import spark.Response;
import spark.Route;
import spark.*;

/**
 * VoteController handles all logic for any vote related requests
 * This design allows for increased testability without stubbing/mocking/actually doing requests
 */
public class VoteController extends App {
    static Route addOneUpvote = (Request req, Response res) -> {
        String mUsername = req.queryParams("mUsername");
        int mPostId = Integer.parseInt(req.params("id"));

        res.status(200);
        res.type("application/json");
        int result = db.addVote(mUsername, mPostId, 1);
        if (result == -1 || result == 0) {
            return gson.toJson(new StructuredResponse("error", "unable to update row " + mPostId, null));
        } else {
            return gson.toJson(new StructuredResponse("ok", "upvoted " + mPostId, result));
        }
    };

    static Route addOneDownvote = (Request req, Response res) -> {
        String mUsername = req.queryParams("mUsername");
        int mPostId = Integer.parseInt(req.params("id"));

        res.status(200);
        res.type("application/json");
        int result = db.addVote(mUsername, mPostId, -1);
        if (result == -1 || result == 0) {
            return gson.toJson(new StructuredResponse("error", "unable to update row " + mPostId, null));
        } else {
            return gson.toJson(new StructuredResponse("ok", "downvoted " + mPostId, result));
        }
    };
}