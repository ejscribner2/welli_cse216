package edu.lehigh.cse216.welli.backend;

import spark.Spark;
import com.google.gson.*;

import net.rubyeye.xmemcached.MemcachedClient;
import net.rubyeye.xmemcached.MemcachedClientBuilder;
import net.rubyeye.xmemcached.XMemcachedClientBuilder;
import net.rubyeye.xmemcached.auth.AuthInfo;
import net.rubyeye.xmemcached.command.BinaryCommandFactory;
import net.rubyeye.xmemcached.exception.MemcachedException;
import net.rubyeye.xmemcached.utils.AddrUtil;

import java.lang.InterruptedException;
import java.net.InetSocketAddress;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

//import java.util.Hashtable;
import java.util.Map;

/**
 * Backend server
 */
public class App {

    static Database db;
    // gson provides us with a way to turn JSON into objects, and objects
    // into JSON.
    // NB: it must be final, so that it can be accessed from our lambdas
    static final Gson gson = new Gson();
    static MemcachedClient mc;
    //static Hashtable<String, String> sessionCache = new Hashtable<>();
    
    public static void main(String[] args) {
        // get the Postgres configuration from the environment
        Map<String, String> env = System.getenv();
        String db_url = env.get("DATABASE_URL");
        String cors_enabled = env.get("CORS_ENABLED");
        
        //memcachier p4 (change server, username, pass from memcachier account)
        /*
        List<InetSocketAddress> servers = AddrUtil.getAddresses(System.getenv("mc5.dev.ec2.memcachier.com:11211").replace(",", " "));
        AuthInfo authInfo = AuthInfo.plain(System.getenv("BC2164"),System.getenv("5A1967DE4AB99BE284BAC8E0328D1894"));*/

        List<InetSocketAddress> servers = AddrUtil.getAddresses("mc5.dev.ec2.memcachier.com:11211");
        AuthInfo authInfo = AuthInfo.plain("BC2164", "5A1967DE4AB99BE284BAC8E0328D1894");

        MemcachedClientBuilder builder = new XMemcachedClientBuilder(servers);

         // Configure SASL auth for each server
        for(InetSocketAddress server : servers) 
        {
            builder.addAuthInfo(server, authInfo);
        }
  
        // Use binary protocol
        builder.setCommandFactory(new BinaryCommandFactory());
        // Connection timeout in milliseconds (default: )
        builder.setConnectTimeout(1000);
        // Reconnect to servers (default: true)
        builder.setEnableHealSession(true);
        // Delay until reconnect attempt in milliseconds (default: 2000)
        builder.setHealSessionInterval(2000);
  
        //MemcachedClient mc;
        
        try {
            mc = builder.build();
            /*try {
                //what am i setting here? username. 
                //how do i choose expiration and key
            //mc.set("username", 0, )
            mc.set("foo", 0, "bar");
            String val = mc.get("foo");
            System.out.println(val);
            } catch (TimeoutException te) {
            System.err.println("Timeout during set or get: " +
                                te.getMessage());
            } catch (InterruptedException ie) {
            System.err.println("Interrupt during set or get: " +
                                ie.getMessage());
            } catch (MemcachedException me) {
            System.err.println("Memcached error during get or set: " +
                                me.getMessage());
            }*/
        } catch (IOException ioe) {
            System.err.println("Couldn't create a connection to Memcached server: " +
                            ioe.getMessage());
        }
        
        

        
        // Config spark
        // Get the port on which to listen for requests
        Spark.port(getIntFromEnv("PORT", 4567));

        // Set up the location for serving static files.  If the STATIC_LOCATION
        // environment variable is set, we will serve from it.  Otherwise, serve
        // from "/web"
        String static_location_override = System.getenv("STATIC_LOCATION");
        if (static_location_override == null) {
            Spark.staticFileLocation("/web");
        } else {
            Spark.staticFiles.externalLocation(static_location_override);
        }

        // Set up a route for serving the main page
        Spark.get("/", (req, res) -> {
            res.redirect("/index.html");
            return "";
        });


        // Get a fully-configured connection to the database, or exit
        // immediately
        db = Database.getDatabase(db_url);
        if (db == null)
            return;

        // check authentication
        //before any protected route it calls that to see if its logined in 
        Spark.before("/protected/*", LoginController.authenticateUser);

        /*
         * Routes for get all, get one, and add one post
         * All logic is handled by AND TESTED IN the PostController
         */
        Spark.get("/protected/posts", PostController.fetchAllPosts);
        Spark.get("/protected/posts/:id", PostController.fetchOnePost);
        Spark.get("/protected/users/topbreeds", PostController.fetchTopBreeds);
        Spark.post("/protected/posts", PostController.addOnePost);

        /*
         * Routes for login/logout/register
         * All logic is handled by AND TESTED IN the LoginController
         */
        Spark.post("/login", LoginController.loginUser);
        Spark.post("/logout", LoginController.logoutUser);
        Spark.post("/register", LoginController.registerUser);

        /*
         * Route for adding a comment
         * All logic is handled by AND TESTED IN the CommentController
         */
        Spark.post("/protected/posts/:id/comment", CommentController.addOneComment);

        /*
         * Routes to handle upvotes and downvotes
         * All logic is handled by AND TESTED IN the VoteController
         */
        Spark.put("/protected/posts/upvotes/:id", VoteController.addOneUpvote);
        Spark.put("/protected/posts/downvotes/:id", VoteController.addOneDownvote);

        /*
         * Routes to handle getting user profile, changing bio, and password
         */
        Spark.get("/protected/users/:username", ProfileController.fetchOneProfile);
        Spark.put("/protected/users/:username/bio", ProfileController.updateBio);
        Spark.put("/protected/users/:username/password", ProfileController.updatePassword);

        /*
         * Route to get all post by one user
         */
         Spark.get("/protected/userPosts/:username", ProfileController.mPostByUser);
    
         if (cors_enabled.equalsIgnoreCase("True")) {
            final String acceptCrossOriginRequestsFrom = "*";
            final String acceptedCrossOriginRoutes = "GET,PUT,POST,DELETE,OPTIONS";
            final String supportedRequestHeaders = "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin";
            enableCORS(acceptCrossOriginRequestsFrom, acceptedCrossOriginRoutes, supportedRequestHeaders);
        }
        }

    /**
     * Get an integer environment varible if it exists, and otherwise return the
     * default value.
     *
     * @envar      The name of the environment variable to get.
     * @defaultVal The integer value to use as the default if envar isn't found
     *
     * @returns The best answer we could come up with for a value for envar
     */
    static int getIntFromEnv(String envar, int defaultVal) {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get(envar) != null) {
            return Integer.parseInt(processBuilder.environment().get(envar));
        }
        return defaultVal;
    }


/**
 * Set up CORS headers for the OPTIONS verb, and for every response that the
 * server sends.  This only needs to be called once.
 * 
 * @param origin The server that is allowed to send requests to this server
 * @param methods The allowed HTTP verbs from the above origin
 * @param headers The headers that can be sent with a request from the above
 *                origin
 */
private static void enableCORS(String origin, String methods, String headers) {
    // Create an OPTIONS route that reports the allowed CORS headers and methods
    Spark.options("/*", (request, response) -> {
        String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
        if (accessControlRequestHeaders != null) {
            response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
        }
        String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
        if (accessControlRequestMethod != null) {
            response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
        }
        return "OK";
    });

    // 'before' is a decorator, which will run before any 
    // get/post/put/delete.  In our case, it will put three extra CORS
    // headers into the response
    Spark.before((request, response) -> {
        response.header("Access-Control-Allow-Origin", origin);
        response.header("Access-Control-Request-Method", methods);
        response.header("Access-Control-Allow-Headers", headers);
    });

    

}


}