package edu.lehigh.cse216.welli.backend;

import spark.Response;
import spark.Route;
import spark.*;

/**
 * CommentController handles all logic for the comment related requests
 * This design allows for increased testability without stubbing/mocking/actually doing requests
 */
public class CommentController extends App {
    static Route addOneComment = (Request req, Response res) -> {
        String mUsername = req.queryParams("mUsername");
        int cId = Integer.parseInt(req.params("id"));
        String cBody = req.queryParams("cBody");

        res.status(200);
        res.type("application/json");
        int ret = db.insertComment(mUsername, cId, cBody);
        if (ret == -1 || ret == 0) {
            return gson.toJson(new StructuredResponse("error", "error commenting", null));
        } else {
            return gson.toJson(new StructuredResponse("ok", "" + ret, null));
        }
    };
}
