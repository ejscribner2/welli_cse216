package edu.lehigh.cse216.welli.backend;

import java.sql.*;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.ArrayList;

class Database {
    /**
     * The connection to the database.  When there is no connection, it should
     * be null.  Otherwise, there is a valid open connection
     */
    private Connection mConnection;

    /**
     * A prepared statement for getting all data from posts, and one
     * for getting all of the vote data
     */
    private PreparedStatement mSelectAllPosts;
    private PreparedStatement mSelectCommentsByPost;

    /**
     * A prepared statement for getting one row from the database
     */
    private PreparedStatement mSelectOne;

    /**
     * A prepared statement for inserting into the database
     */
    private PreparedStatement mInsertOneUser;
    private PreparedStatement mInsertOnePost;
    private PreparedStatement mInsertOneUpvotes;
    private PreparedStatement mInsertOneDownvotes;
    private PreparedStatement mInsertOneComments;
    private PreparedStatement mInsertFile;

    /**
     * Prepared statements for validating
     */
    private PreparedStatement mValidateUserReg;
    private PreparedStatement mValidateChatUser;
    private PreparedStatement mValidatePost;

    /**
     * Prepared statements for authentication
     */
    private PreparedStatement mGetHash;
    private PreparedStatement mGetUserId;

    /**
     * Prepared statements for user profiles
     */
    private PreparedStatement mGetUserProf;
    private PreparedStatement mUpdateUserBio;
    private PreparedStatement mUpdateUserPassword;

    //ADDED P3
    private PreparedStatement mPostByUser;

    private PreparedStatement mTopBreeds;

    /**
     * CommentData acts as a data structure to hold all data associated with a
     * comment object
     */
    static class CommentData {
        /**
         * The ID of this comment
         */
        int cId;
        /**
         * The sender of this comment
         */
        String cSender;
        /**
         * The comment's contents
         */
        String cComment;
        /**
         * The date it was commented
         */
        String cDate;

        /**
         * Construct a CommentData with the:
         *
         * @param id      of comment
         * @param sender  of comment
         * @param comment contents
         * @param date    comment sent
         */
        CommentData(int id, String sender, String comment, String date) {
            cId = id;
            cSender = sender;
            cComment = comment;
            cDate = date;
        }
    }


    /**
     * PostData is like a struct in C: we use it to hold data, and we allow
     * direct access to its fields.  In the context of this Database, PostData
     * represents the data we'd see in a row.
     * <p>
     * We make PostData a static class of Database because we don't really want
     * to encourage users to think of PostData as being anything other than an
     * abstract representation of a row of the database.  PostData and the
     * Database are tightly coupled: if one changes, the other should too.
     */
    static class PostData {
        /**
         * The ID of this post
         */
        int mId;
        /**
         * The sender of the post
         */
        String mUsername;
        /**
         * The title of the post
         */
        String mTitle;
        /**
         * The body of the post stored in this row
         */
        String mBody;
        /**
         * The date it was posted
         */
        String mDate;
        String mBreed;
        /**
         * The number of upvotes for this post
         */
        int mUpvotes;
        /**
         * The number of downvotes for this post
         */
        int mDownvotes;
        /**
         * The comments associated with the post
         */
        ArrayList<CommentData> mComments;

        /**
         * The url to the file if present
         */
        String mFile;

        /**
         * Construct a PostData object by providing values for its fields
         */
        PostData(int id, String username, String title, String body, String date, String breed, int upvotes, int downvotes, ArrayList<CommentData> comments, String file) {
            mId = id;
            mUsername = username;
            mTitle = title;
            mBody = body;
            mDate = date;
            mBreed = breed;
            mUpvotes = upvotes;
            mDownvotes = downvotes;
            mComments = comments;
            mFile = file;
        }
    }

    /**
     * CommentData acts as a data structure to hold all data associated with a
     * comment object
     */
    static class ProfileData {
        /**
         * The users first name
         */
        String mFirstname;
        /**
         * The users last name
         */
        String mLastname;
        /**
         * The username
         */
        String mUsername;
        /**
         * The email of the user
         */
        String mEmail;
        /**
         * The bio of the user
         */
        String mBio;


        ProfileData(String firstName, String lastName, String username, String email, String bio) {
            mFirstname = firstName;
            mLastname = lastName;
            mUsername = username;
            mEmail = email;
            mBio = bio;
        }
    }

    static class TopBreedData{
        String mBreed;
        int mNumPosts;

        TopBreedData(String breed, int numPosts){
            mBreed = breed;
            mNumPosts = numPosts;
        }
    }

    /**
     * The Database constructor is private: we only create Database objects
     * through the getDatabase() method.
     */
    private Database() {
    }

    /**
     * Get a fully-configured connection to the database
     *
     * @param db_url The url the database server
     * @return A Database object, or null if we cannot connect properly
     */
    static Database getDatabase(String db_url) {
        // Create an un-configured Database object
        Database db = new Database();

        // Give the Database object a connection, fail if we cannot get one
        try {
            Class.forName("org.postgresql.Driver");
            URI dbUri = new URI(db_url);
            String username = dbUri.getUserInfo().split(":")[0];
            String password = dbUri.getUserInfo().split(":")[1];
            String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath() + "?sslmode=require";
            //Connection conn = DriverManager.getConnection("jdbc:postgresql://" + dbUri.getHost() + ":" + dbUri.getPort() + "/" + dbUri.getPath() + "?sslmode=require", username, password); 
            Connection conn = DriverManager.getConnection(dbUrl, username, password);
            if (conn == null) {
                System.err.println("Error: DriverManager.getConnection() returned a null object");
                return null;
            }
            db.mConnection = conn;
        } catch (SQLException e) {
            System.err.println("Error: DriverManager.getConnection() threw a SQLException");
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException cnfe) {
            System.out.println("Unable to find postgresql driver");
            return null;
        } catch (URISyntaxException s) {
            System.out.println("URI Syntax Error");
            return null;
        }

        // Attempt to create all of our prepared statements.  If any of these
        // fail, the whole getDatabase() call should fail
        try {
            // NB: we can easily get ourselves in trouble here by typing the
            //     SQL incorrectly.  We really should have things like "tblData"
            //     as constants, and then build the strings for the statements
            //     from those constants.

            // Note: no "IF NOT EXISTS" or "IF EXISTS" checks on table
            // creation/deletion, so multiple executions will cause an exception

            // Standard CRUD operations
            db.mSelectAllPosts = db.mConnection.prepareStatement("select DISTINCT post.p_id,\n" +
                    "                username,\n" +
                    "                title,\n" +
                    "                post.body,\n" +
                    "                post.breed,\n" +
                    "                date,\n" +
                    "                count(u) as upvotes,\n" +
                    "                count(d) as downvotes,\n" +
                    "                url as fileUrl\n" +
                    "from post\n" +
                    "  inner join chat_users cu on post.sender_id = cu.u_id\n" +
                    "  left join upvotes u on post.p_id = u.p_id\n" +
                    "  left join downvotes d on post.p_id = d.p_id\n" +
                    "  left join file f on post.p_id = f.post_id\n" +
                    "group by post.p_id, cu.username, fileUrl;");
            db.mSelectCommentsByPost = db.mConnection.prepareStatement("select c_id, username as sender, body as comment, date\n" +
                    "from comment inner join chat_users cu on comment.sender_id = cu.u_id\n" +
                    "where post_id = ?;");
            db.mSelectOne = db.mConnection.prepareStatement("select DISTINCT post.p_id,\n" +
                    "                username,\n" +
                    "                title,\n" +
                    "                post.body,\n" +
                    "                post.breed,\n" +
                    "                date,\n" +
                    "                count(u) as upvotes,\n" +
                    "                count(d) as downvotes\n" +
                    "from post\n" +
                    "  inner join chat_users cu on post.sender_id = cu.u_id\n" +
                    "  left join upvotes u on post.p_id = u.p_id\n" +
                    "  left join downvotes d on post.p_id = d.p_id\n" +
                    "where post.p_id = ?\n" +
                    "group by post.p_id, cu.username;");
            // NB: The null value below can be changed to a 'default' message string or a field a user enters when they sign up
            db.mInsertOneUser = db.mConnection.prepareStatement("insert into chat_users values(" +
                    " default,\n" +
                    " ?,\n" +
                    " ?,\n" +
                    " ?,\n" +
                    " ?,\n" +
                    "null,\n" +
                    ");");
            db.mInsertOnePost = db.mConnection.prepareStatement("insert into post values(" +
                    "default,\n" +
                    " ?,\n" +
                    " ?,\n" +
                    " ?,\n" +
                    " DEFAULT,\n" +
                    " ?\n" +
                    ");", Statement.RETURN_GENERATED_KEYS);
            db.mInsertOneUpvotes = db.mConnection.prepareStatement("insert into upvotes values\n" +
                    "(\n" +
                    "?,\n" +
                    "?\n" +
                    ")\n" +
                    "on conflict ON CONSTRAINT upvotes_pkey do nothing;");
            db.mInsertOneDownvotes = db.mConnection.prepareStatement("insert into downvotes values\n" +
                    "(\n" +
                    "?,\n" +
                    "?\n" +
                    ")\n" +
                    "on conflict ON CONSTRAINT downvotes_pkey do nothing;");
            db.mInsertOneComments = db.mConnection.prepareStatement("insert into comment values\n" +
                    "(\n" +
                    " default,\n" +
                    " ?,\n" +
                    " ?,\n" +
                    " ?,\n" +
                    " DEFAULT\n" +
                    ");");
            db.mValidateUserReg = db.mConnection.prepareStatement("Select u_id from chat_users where username = ? and email = ?");
            db.mValidateChatUser = db.mConnection.prepareStatement("Select u_id from chat_users where username = ?");
            db.mValidatePost = db.mConnection.prepareStatement("select p_id from post where p_id = ?");

            // auth
            db.mGetHash = db.mConnection.prepareStatement("select passHash from chat_users where username = ?");
            db.mGetUserId = db.mConnection.prepareStatement("select u_id from chat_users where username = ?");

            // user profiles
            db.mGetUserProf = db.mConnection.prepareStatement("select firstname,\n" +
                    "       lastname,\n" +
                    "       username,\n" +
                    "       email,\n" +
                    "       bio\n" +
                    "from chat_users\n" +
                    "where username = ?;");
            db.mUpdateUserBio = db.mConnection.prepareStatement("update chat_users\n" +
                    "set bio = ?\n" +
                    "where username = ?;");
            db.mUpdateUserPassword = db.mConnection.prepareStatement("update chat_users\n" +
                    "set salt = ?,\n" +
                    "    passHash = ?\n" +
                    "where username = ?;");

            //ADDED P3

            db.mPostByUser = db.mConnection.prepareStatement("select DISTINCT post.p_id,\n" +
            "                username,\n" +
            "                title,\n" +
            "                post.body,\n" +
            "                post.breed,\n" +
            "                date,\n" +
            "                count(u) as upvotes,\n" +
            "                count(d) as downvotes\n" +
            "from post\n" +
            "  inner join chat_users cu on post.sender_id = cu.u_id\n" +
            "  left join upvotes u on post.p_id = u.p_id\n" +
            "  left join downvotes d on post.p_id = d.p_id\n" +
            "where username = ?" +
            "group by post.p_id, cu.username;");

            //last phase

            db.mTopBreeds = db.mConnection.prepareStatement("select breed, count(breed) as count " +
                    "from post " +
                    "group by breed " +
                    "order by count DESC " +
                    "limit 3;");


            db.mInsertFile = db.mConnection.prepareStatement("" +
                    "insert into file values\n" +
                    "(\n" +
                    " '12345',\n" +
                    " ?, -- post id\n" +
                    " 'repost_image.jpg',\n" +
                    " 'image',\n" +
                    " ?,  -- url\n" +
                    " DEFAULT,\n" +
                    " DEFAULT,\n" +
                    " 1,\n" +
                    " false\n" +
                    ");");

        } catch (SQLException e) {
            System.err.println("Error creating prepared statement");
            e.printStackTrace();
            db.disconnect();
            return null;
        }
        return db;
    }

    /**
     * Close the current connection to the database, if one exists.
     * <p>
     * NB: The connection will always be null after this call, even if an
     * error occurred during the closing operation.
     *
     * @return True if the connection was cleanly closed, false otherwise
     */
    boolean disconnect() {
        if (mConnection == null) {
            System.err.println("Unable to close connection: Connection was null");
            return false;
        }
        try {
            mConnection.close();
        } catch (SQLException e) {
            System.err.println("Error: Connection.close() threw a SQLException");
            e.printStackTrace();
            mConnection = null;
            return false;
        }
        mConnection = null;
        return true;
    }

    //add to res.add to get file !!
    /**
     * Query the database for a list of all subjects and their IDs
     *
     * @return All posts, as an ArrayList
     */
    ArrayList<PostData> selectAll() {
        ArrayList<PostData> res = new ArrayList<PostData>();
        try {
            ResultSet postData = mSelectAllPosts.executeQuery();
            while (postData.next()) {
                res.add(new PostData(postData.getInt("p_id"), postData.getString("username"), postData.getString("title"), postData.getString("body"), postData.getString("date"), postData.getString("breed"), postData.getInt("upvotes"), postData.getInt("downvotes"), null, postData.getString("fileUrl")));
            }
            postData.close();
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }


    //ADDED P3
    ArrayList<PostData> selectPostByUser(String username) {
        ArrayList<PostData> res = new ArrayList<PostData>();
        try {
            mPostByUser.setString(1, username);
            ResultSet postData = mPostByUser.executeQuery();
            while (postData.next()) {
                res.add(new PostData(postData.getInt("p_id"), postData.getString("username"), postData.getString("title"), postData.getString("body"), postData.getString("date"), postData.getString("breed"), postData.getInt("upvotes"), postData.getInt("downvotes"), null, null));
            }
            postData.close();
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }


    /**
     * Get all data for a specific row, by ID
     *
     * @param id of the row being requested
     * @return data for the requested row, or null if the ID was invalid
     */
    //add to res.add to get file !!
    PostData selectOne(int id) {
        PostData res = null;
        try {
            mSelectOne.setInt(1, id);
            ResultSet rs = mSelectOne.executeQuery();
            if (rs.next()) {
                res = new PostData(rs.getInt("p_id"), rs.getString("username"), rs.getString("title"), rs.getString("body"), rs.getString("date"), rs.getString("breed"), rs.getInt("upvotes"), rs.getInt("downvotes"), null, null);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Get comments for posts
     *
     * @param id of a post
     * @return an ArrayList of CommentData for the post
     */
    ArrayList<CommentData> selectComments(int id) {
        ArrayList<CommentData> res = new ArrayList<CommentData>();
        try {
            mSelectCommentsByPost.setInt(1, id);
            ResultSet rs = mSelectCommentsByPost.executeQuery();
            while (rs.next()) {
                res.add(new CommentData(rs.getInt("c_id"), rs.getString("sender"), rs.getString("comment"), rs.getString("date")));
            }
            rs.close();
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @param username of user to look up
     * @return the uses profile
     */
    ProfileData selectOneUser(String username) {
        ProfileData res = null;
        try {
            mGetUserProf.setString(1, username);
            ResultSet rs = mGetUserProf.executeQuery();
            if (rs.next()) {
                res = new ProfileData(rs.getString("firstname"), rs.getString("lastname"), rs.getString("username"), rs.getString("email"), rs.getString("bio"));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * To register/insert a new user
     *
     * @param username  of new user
     * @param firstName of new user
     * @param lastName  of new user
     * @param passHash  the hash of the password given by new user
     * @return response
     */
    int insertUserRow(String username, String firstName, String lastName, String email) {
        int count = 0;
        int exists = 0;
        try {
            mValidateUserReg.setString(1, username);
            mValidateUserReg.setString(2, email);
            ResultSet rs = mValidateUserReg.executeQuery();
            if (rs.next()) {
                exists = rs.getInt("u_id");
            }
            rs.close();
            if (exists == 0) {
                mInsertOneUser.setString(1, username);
                mInsertOneUser.setString(2, firstName);
                mInsertOneUser.setString(3, lastName);
                mInsertOneUser.setString(4, email);
                count += mInsertOneUser.executeUpdate();
            } else {
                System.err.println("looks like that user already exists");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    int updateUserPassword(String username, String newSalt, String newPassHash) {
        int count = 0;
        int exists = 0;
        System.out.println("username is: " + username + " \n newSalt is: " + newSalt + " \n newPassHash is: " + newPassHash +" \n");
        try {
            mValidateChatUser.setString(1, username);
            ResultSet rs = mValidateChatUser.executeQuery();
            if (rs.next()) {
                exists = rs.getInt("u_id");
            }
            rs.close();
            if (exists != 0) {
                mUpdateUserPassword.setString(1, newSalt);
                mUpdateUserPassword.setString(2, newPassHash);
                mUpdateUserPassword.setString(3, username);
                count += mUpdateUserPassword.executeUpdate();
            } else {
                System.err.println("could not update password");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * Inserts a comment for a given post
     *
     * @param username of comment poster
     * @param id       of comment
     * @param body     of comment
     * @return response
     */
    int insertComment(String username, int id, String body) {
        int count = 0;
        int exists = 0;
        try {
            mValidateChatUser.setString(1, username);
            ResultSet rs = mValidateChatUser.executeQuery();
            if (rs.next()) {
                exists = rs.getInt("u_id");
            }
            rs.close();
            if (exists != 0) {
                mInsertOneComments.setInt(1, id);
                mInsertOneComments.setInt(2, exists);
                mInsertOneComments.setString(3, body);
                count += mInsertOneComments.executeUpdate();
            } else {
                System.err.println("user not validated");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * Get the hashed password stored in the database for a user
     *
     * @param username of the user
     * @return the hashed password associated with said user
     */
    String getPassHash(String username) {
        String res = null;
        try {
            mGetHash.setString(1, username);
            ResultSet rs = mGetHash.executeQuery();
            if (rs.next()) {
                res = rs.getString("passhash");
            } else {
                System.err.println("looks like that username or password does not exist");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Get the ID of a user given their username
     *
     * @param username of user
     * @return corresponding ID
     */
    int getUserId(String username) {
        int id = -1;
        try {
            mGetUserId.setString(1, username);
            ResultSet rs = mGetUserId.executeQuery();
            if (rs.next()) {
                id = rs.getInt("u_id");
            } else {
                System.err.println("looks like that username or password does not exist");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    /**
     * Add a new post
     *
     * @param username of poster
     * @param title    of post
     * @param body     of post
     * @param file     of post //added p4
     * @return response
     */
    int insertPostRow(String username, String title, String body, String breed, String url) {
        int count = 0;
        int user = 0;
        try {
            mValidateChatUser.setString(1, username);
            ResultSet rs = mValidateChatUser.executeQuery();
            if (rs.next()) {
                user = rs.getInt("u_id");
            }
            rs.close();
            if (user != 0) {
                mInsertOnePost.setInt(1, user);
                mInsertOnePost.setString(2, title);
                mInsertOnePost.setString(3, body);
                mInsertOnePost.setString(4, breed);
                count += mInsertOnePost.executeUpdate();
            } else {
                System.err.println("Looks like that user doesnt exist, so they cant make a post");
            }
            ResultSet insertResult = mInsertOnePost.getGeneratedKeys();
            int lastInserted = -1;
            if(insertResult.next()) {
                lastInserted = insertResult.getInt(1);
            }
            System.out.println("last inserted is: " + lastInserted);
            if(url != null) {
                mInsertFile.setInt(1, lastInserted);
                mInsertFile.setString(2, url);
                mInsertFile.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * Adds a new vote (handles both up or down)
     *
     * @param username of voter
     * @param postId   being voted on
     * @param dir      direction of vote (-1 or 1)
     * @return response
     */
    int addVote(String username, int postId, int dir) {
        int count = 0;
        int userExists = 0;
        int postExists = 0;
        try {
            mValidateChatUser.setString(1, username);
            ResultSet rs = mValidateChatUser.executeQuery();
            if (rs.next()) {
                userExists = rs.getInt("u_id");
            }
            rs.close();
            if (userExists != 0) {
                mValidatePost.setInt(1, postId);
                rs = mValidatePost.executeQuery();
                if (rs.next()) {
                    postExists = rs.getInt("p_id");
                }
                rs.close();
                if (postExists != 0) {
                    if (dir == 1) {
                        // add an upvote
                        mInsertOneUpvotes.setInt(1, userExists);
                        mInsertOneUpvotes.setInt(2, postExists);
                        count += mInsertOneUpvotes.executeUpdate();
                    }
                    if (dir == -1) {
                        // add a downvote
                        mInsertOneDownvotes.setInt(1, userExists);
                        mInsertOneDownvotes.setInt(2, postExists);
                        count += mInsertOneDownvotes.executeUpdate();
                    }
                    if (dir == 0 || dir > 1 || dir < -1) {
                        System.out.println("vote direction param out of bounds");
                    }
                } else {
                    System.err.println("Looks like that post doesnt exist, so you cant like it");
                }

            } else {
                System.err.println("Looks like that user doesnt exist, so they cant like a post");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    int updateBio(String username, String bio) {
        int res = 0;
        int userExists = 0;
        try {
            mValidateChatUser.setString(1, username);
            ResultSet rs = mValidateChatUser.executeQuery();
            if (rs.next()) {
                userExists = rs.getInt("u_id");
            }
            rs.close();
            if(userExists != 0) {
                mUpdateUserBio.setString(1, bio);
                mUpdateUserBio.setString(2, username);
                res = mUpdateUserBio.executeUpdate();
            } else {
                System.err.println("Looks like that user doesnt exist");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    ArrayList<TopBreedData> getTopBreeds() {
        ArrayList<TopBreedData> res = new ArrayList<TopBreedData>();
        try {
            ResultSet breedData = mTopBreeds.executeQuery();
            while (breedData.next()) {
                res.add(new TopBreedData(breedData.getString("breed"), breedData.getInt("count")));
            }
            breedData.close();
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}