package edu.lehigh.cse216.welli.backend;

import com.sun.org.apache.xpath.internal.operations.Bool;

import net.rubyeye.xmemcached.MemcachedClient;
import spark.Response;
import spark.Route;
import spark.*;

import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.Collections;

import javax.lang.model.util.ElementScanner6;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;


    
/**
 * LoginController handles all logic for login/auth related requests
 * This design allows for increased testability without stubbing/mocking/actually doing requests
 */
public class LoginController extends App {

        
    // probably should be removed to avoid vulnurabilities
    static Route registerUser = (Request req, Response res) -> { // needs to be updated for when the users info isnt already in the db
        String mUsername = req.queryParams("mUsername");
//        String mPassword = req.queryParams("mPassword");
        String mFirstName = req.queryParams("mFirstName");
        String mLastName = req.queryParams("mLastName");
        String mEmail = req.queryParams("mEmail");

        res.status(200);
        res.type("application/json");
        //String salt = BCrypt.gensalt(12);
//        String passHash = BCrypt.hashpw(mPassword, salt);
        int newId = db.insertUserRow(mUsername, mFirstName, mLastName, mEmail);
        if (newId == -1 || newId == 0) {
            return gson.toJson(new StructuredResponse("error", "error registering user", null));
        } else {
            return gson.toJson(new StructuredResponse("ok", "" + newId, null));
        }
    };

    private static boolean insertUser(String username, String firstName, String lastName, String email) {
        int newId = db.insertUserRow(username, firstName, lastName, email);
        if (newId == -1 || newId == 0) {
            return false;
        } else {
            return true;
        }
    }

    

    static Route loginUser = (Request req, Response res) -> {

        System.out.println("got into loginUer"); 

        String loginToken = req.queryParams("mToken");
        System.out.println("*****    " + loginToken); 
        
        String email = null;
        String mUsername = null;
        /*
        HttpTransport transport = new NetHttpTransport();
        JacksonFactory jacksonFactory = new JacksonFactory();
        */

         HttpTransport transport = new NetHttpTransport();
         JsonFactory jsonFactory = new JacksonFactory();

        System.out.println("here 1"); 

        res.status(200);
        res.type("application/json");
      
        System.out.println("here 2"); 
        
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
        .setAudience(Collections.singletonList("171832678069-hi1cspmrc9el18424n7rfo58bfk81gtf.apps.googleusercontent.com"))
        .build();

        System.out.println("here 3"); 

        // (Receive idTokenString by HTTPS POST)
    
        GoogleIdToken idToken = verifier.verify(loginToken);
        System.out.println("here 4");

        String lastName = null;
        String firstName = null;
        if (idToken != null) {

            Payload payload = idToken.getPayload();

            System.out.println("here 5");

            // Print user identifier
            String userId = payload.getSubject();
            System.out.println("User ID: " + userId);

            System.out.println("here 6");

            // Get profile information from payload
            email = payload.getEmail();
//        boolean emailVerified = Boolean.valueOf(payload.getEmailVerified());
            String name = (String) payload.get("name");
            System.out.println("name is: " + name);
            String[] names = name.split(" ");
            firstName = names[0];
            lastName = names[1];
//        String familyName = (String) payload.get("family_name");
//        String givenName = (String) payload.get("given_name");

            mUsername = email;

        } else {
            System.out.println("Invalid ID token.");
        }
        System.out.println("******" ); 
        //if(email.contains("lehigh.edu")){ // is this necessary?

            Database.ProfileData userLoggingIn = db.selectOneUser(email);
            if(userLoggingIn == null) {
                System.out.println("Add to db!");
                Boolean inserted = insertUser(email, firstName, lastName, email);
                if (inserted) {
                    System.out.println("successfully added!");
                } else {
                    return gson.toJson(new StructuredResponse("error", "error adding user to db", null));
                }
            } else {
                System.out.println("continue, already in db. Welcome back!");
            }


            // add to cache
            //sessionCache.put(mUsername, loginToken);

            //add to memcache or set? exp?
            mc.set(mUsername, 90, loginToken);
            
            // on successful login set cookie
            return gson.toJson(new StructuredResponse("ok", "" + loginToken, null));

            
       /* } else {
            return gson.toJson(new StructuredResponse("error", "error logging in", null));
        }*/
        
    };

    static Filter authenticateUser = (Request req, Response res) -> {
        String username = req.queryParams("mUsername");
        String token = req.queryParams("mToken");
        // checks if user exists and if user is auth'd
        if (username == null) {
            Spark.halt(422, "No 'mUsername' parameter passed");
        } else if(token == null) {
            Spark.halt(401, "No 'mToken' parameter passed");
        } else if(mc.get(username) == null) {
            Spark.halt(401, "Session not valid");
        }else if(mc.get(username).equals(token)) {
            System.out.println("Logged in");
        } else {
            Spark.halt(401, "Not auth'd :(");
        }
    };

    static Route logoutUser = (Request req, Response res) -> {
        String mUsername = req.queryParams("mUsername");
        res.status(200);
        res.type("application/json");

        /*String userLoggedOut = sessionCache.remove("mUsername");
        if (userLoggedOut != null) {
            return gson.toJson(new StructuredResponse("ok", "logged out: " + userLoggedOut, null));
        } else {
            return gson.toJson(new StructuredResponse("error", "error session not found", null));
        }*/
        // memcache remove 
        boolean userLoggedOut = mc.delete(mUsername);
        if(userLoggedOut)
        {
            return gson.toJson(new StructuredResponse("ok", "logged out: " + userLoggedOut, null));
        }
        else 
        {
            return gson.toJson(new StructuredResponse("error", "error session not found", null));
        }
    };
}
