package edu.lehigh.cse216.welli.backend;

import java.util.ArrayList;

import spark.*;

/**
 * LoginController handles all logic for login/auth related requests
 * This design allows for increased testability without stubbing/mocking/actually doing requests
 */
public class ProfileController extends App {

//fetch post by profile

    /**
     * TODO: - PUT Edit Bio
     * TODO: - Put Change Password
     */
    public static Route fetchOneProfile = (Request req, Response res) -> {
        String mUsername = req.queryParams("mUsername");
        String profileUsername = req.params("username");
        System.out.println("fething profile");
        res.status(200);
        res.type("application/json");
        Database.ProfileData data = db.selectOneUser(profileUsername);
        if (data == null) {
            return gson.toJson(new StructuredResponse("error", profileUsername + " not found", null));
        } else {
            return gson.toJson(new StructuredResponse("ok", null, data));
        }
    };

    static Route updateBio = (Request req, Response res) -> {
        String mUsername = req.queryParams("mUsername");
        String userToUpdate = req.params("username");
        String mBio = req.queryParams("mBio");


        res.status(200);
        res.type("application/json");
        if(!userToUpdate.equals(mUsername)) {
            return gson.toJson(new StructuredResponse("error", "cannot update another users post", null));
        }
        int result = db.updateBio(mUsername, mBio);
        if (result == -1 || result == 0) {
            return gson.toJson(new StructuredResponse("error", "unable to update bio for " + mUsername, null));
        } else {
            return gson.toJson(new StructuredResponse("ok", "" + result, result));
        }
    };

    static Route updatePassword = (Request req, Response res) -> {
        String mUsername = req.queryParams("mUsername");
        String userToUpdate = req.params("username");
        String mPassword = req.queryParams("mPassword");

        res.status(200);
        res.type("application/json");
        if(!userToUpdate.equals(mUsername)) {
            return gson.toJson(new StructuredResponse("error", "cannot change another users password", null));
        }
        String salt = BCrypt.gensalt(12);
        String passHash = BCrypt.hashpw(mPassword, salt);
        int result = db.updateUserPassword(mUsername, salt, passHash);
        if (result == -1 || result == 0) {
            return gson.toJson(new StructuredResponse("error", "unable to update password for " + mUsername, null));
        } else {
            return gson.toJson(new StructuredResponse("ok", "" + result, result));
        }
    };

    //ADDED P3
    //get all the post by one user

    public static Route mPostByUser = (Request req, Response res) -> {
        String profileUsername = req.params("username");

        System.out.println("fetching post by user");
        res.status(200);
        res.type("application/json");
        
        ArrayList<Database.PostData> data = db.selectPostByUser(profileUsername);
        
         if (data == null) {
            return gson.toJson(new StructuredResponse("error", profileUsername + " not found", null));
        } else {
            return gson.toJson(new StructuredResponse("ok", null, data));
        }
       
    };

}
