/*
    Created By: Elliot J Scribner on 2019-10-12
    Student ID: ejs320
    Lab #: **Num**
    PostController: **Description**
 */

package edu.lehigh.cse216.welli.backend;

import spark.Response;
import spark.Route;

import java.io.File;

import com.google.api.client.http.FileContent;
import com.google.api.services.drive.model.FileList;


import spark.*;

public class PostController extends App{

    public static Route fetchAllPosts = (Request request, Response response) -> {
        response.status(200);
        response.type("application/json");
        return gson.toJson(new StructuredResponse("ok", null, db.selectAll()));
    };

    public static Route fetchOnePost = (Request request, Response response) -> {
        int idx = Integer.parseInt(request.params("id"));

        response.status(200);
        response.type("application/json");
        Database.PostData data = db.selectOne(idx);
        data.mComments = db.selectComments(data.mId);
        if (data == null) {
            return gson.toJson(new StructuredResponse("error", idx + " not found", null));
        } else {
            return gson.toJson(new StructuredResponse("ok", null, data));
        }
    };

    public static Route addOnePost = (Request request, Response response) -> {
        String mUsername = request.queryParams("mUsername");
        String mTitle = request.queryParams("mTitle");
        String mBody = request.queryParams("mBody");
        String mBreed = request.queryParams("mBreed");
        //insert file into its own table, new ps
        String mFile = request.queryParams("mFile");
        

        //File fileMetadata = new File();
        //fileMetadata.setName("photo.jpg");
        //java.io.File filePath = new java.io.File("files/photo.jpg");
       // FileContent mediaContent = new FileContent("image/jpeg", filePath);
        //File file = driveService.files().create(fileMetadata, mediaContent)
        //.setFields("id")
        //.execute();
        //System.out.println("File ID: " + file.getId());

        response.status(200);
        response.type("application/json");
        int ret = db.insertPostRow(mUsername, mTitle, mBody, mBreed, mFile);
        if (ret == -1 || ret == 0) {
            return gson.toJson(new StructuredResponse("error", "error adding post", null));
        } else {
            return gson.toJson(new StructuredResponse("ok", "" + ret, null));
        }
    };

    public static Route fetchTopBreeds = (Request request, Response response) -> {
        response.status(200);
        response.type("application/json");
        return gson.toJson(new StructuredResponse("ok", null, db.getTopBreeds()));
    };

    
}
