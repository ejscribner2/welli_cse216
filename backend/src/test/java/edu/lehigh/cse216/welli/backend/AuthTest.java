/*
    Created By: Elliot J Scribner on 2019-10-12
    Student ID: ejs320
    Lab #: **Num**
    PostTest: **Description**
*/
/*
package edu.lehigh.cse216.welli.backend;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.net.URISyntaxException;
*/

/**
 * Testing for all 'posts' related functionality in the backend
 
public class AuthTest extends TestCase {*/

    /**
     * Construct the test case
     * @param testName name of the test case
     
    public AuthTest(String testName) {
        super(testName);
    }*/

    /**
     * @return the suite of tests being tested
     
    public static Test suite() {
        return new TestSuite(AuthTest.class);
    }*/

    /**
     * Test for user login
     
    public void testUserLogin() throws IOException, URISyntaxException {
        URIBuilder authUri = new URIBuilder("https://infinite-island-04530.herokuapp.com/login");
        authUri.setParameter("mUsername", "TEST_4");
        authUri.setParameter("mPassword", "helloworld");
        HttpPost authReq = new HttpPost(authUri.build());
        HttpResponse authRes = HttpClientBuilder.create().build().execute(authReq);

        assertEquals(200, authRes.getStatusLine().getStatusCode());
    }
}*/
