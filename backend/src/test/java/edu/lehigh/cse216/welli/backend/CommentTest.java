/*
    Created By: Elliot J Scribner on 2019-10-12
    Student ID: ejs320
    Lab #: **Num**
    PostTest: **Description**
 */

package edu.lehigh.cse216.welli.backend;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


/**
 * Testing for all 'posts' related functionality in the backend
 */
public class CommentTest extends TestCase {
    Database.CommentData testComment;

    /**
     * Construct the test case
     * @param testName name of the test case
     */
    public CommentTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(CommentTest.class);
    }

    /**
     * Test for creation of CommentData
     */
    public void testCommentData() {
        int id = 1;
        String sender = "joemama";
        String comment = "this is a good post";
        String date = "2019-10-09 19:58:18.250760";

        testComment = new Database.CommentData(id, sender, comment, date);

        assertEquals(testComment.cId, id);
        assertEquals(testComment.cSender, sender);
        assertEquals(testComment.cComment, comment);
        assertEquals(testComment.cDate, date);
    }
}
