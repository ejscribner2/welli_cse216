/*
    Created By: Elliot J Scribner on 2019-10-12
    Student ID: ejs320
    Lab #: **Num**
    PostTest: **Description**
 */

package edu.lehigh.cse216.welli.backend;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


/**
 * Testing for all 'posts' related functionality in the backend
 */
public class PostTest extends TestCase {
    Database.PostData testPost;


    /**
     * Construct the test case
     * @param testName name of the test case
     */
    public PostTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(PostTest.class);
    }

    /**
     * Test for creation of PostData
     */
    public void testPostData() {
        int id = 216;
        String username = "joemama";
        String title = "myTestPost";
        String body = "hello everyone, this is my first post!";
        String date = "2019-10-09 19:58:18.250760";
        String breed = "poodle";
        int upvotes = 21;
        int downvotes = 6;
        testPost = new Database.PostData(id, username, title, body, date, breed, upvotes, downvotes, null, null);

        assertEquals(testPost.mId, id);
        assertEquals(testPost.mUsername, username);
        assertEquals(testPost.mTitle, title);
        assertEquals(testPost.mBody, body);
        assertEquals(testPost.mDate, date);
        assertEquals(testPost.mUpvotes, upvotes);
        assertEquals(testPost.mDownvotes, downvotes);
        assertNull(testPost.mComments);
    }
}

/*
    public void testUserLogin() throws IOException, URISyntaxException {
        URIBuilder authUri = new URIBuilder("https://infinite-island-04530.herokuapp.com/login");
        authUri.setParameter("mUsername", "TEST_3");
        authUri.setParameter("mPassword", "helloworld");
        HttpPost authReq = new HttpPost(authUri.build());
        HttpResponse authRes = HttpClientBuilder.create().build().execute(authReq);

        URIBuilder commentUri = new URIBuilder("https://infinite-island-04530.herokuapp.com/protected/posts/7");
        commentUri.setParameter("mUsername", "TEST_3");
        commentUri.setParameter("body", "comment from testing");
        HttpUriRequest req = new HttpGet(commentUri.build());
        HttpResponse res = HttpClientBuilder.create().build().execute(req);

        assertEquals(200, authRes.getStatusLine().getStatusCode()); // change from 200?
        assertEquals(200, res.getStatusLine().getStatusCode()); // change from 200?
    }
 */
