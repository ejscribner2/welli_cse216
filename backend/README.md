# Backend Readme
All documentation for the current backend routes can be found at: https://documenter.getpostman.com/view/4459461/SVtZtk4f?version=latest

## Backend Update Notes
- The App.java file is now abstracted somewhat into controllers for the different features within the app:
    - LoginController.java
        - Contains logic for `registerUser`, `loginUser`, `authenticateUser`, and `logoutUser`
        - Used in the *Post(/login)*, *Post(/logout)*, *Post(/register)*, and *Before(/protected)* routes
    - PostController.java
        - Contains logic for `fetchAllPosts`, `fetchOnePost`, and `addOnePost`
        - Used in the *Get(/protected/posts)*, *Get(/protected/posts/:id)*, and *Post(/protected/posts)* routes
    - CommentController.java
        - Contains logic for `addOneComment`
        - Used in the *Post(/protected/posts/:id/comment)* route
    - VoteController.java
        - Contains logic for `addOneUpvote` and `addOneDownvote`
        - Used in the *Put(protected/posts/upvotes/:id)*, *Put(protected/posts/downvotes/:id)* routes
    - ProfileController.java
        - Contains logic for `fetchOneProfile`, `updateBio`, and `updatePassword`
        - Used in the *Get(/protected/users/:username)*, *Put(/protected/users/:username/bio)* , *Put(/protected/users/:username/password)* routes
- The routes are set up in the following format, and make calls to their respective controllers accordingly
    ```
    Spark.get("/protected/posts", PostController.fetchAllPosts);
    ```
