package edu.lehigh.cse216.welli.admin;

import java.util.Map;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     * THIS TEST WAS NEVER UPDATED TO WORK WITH THE NEW PHASE 2 MODEL
     * DO NOT DELETE THIS TEST OR FILE, FIX THE TEST!!
     * --Elliot
     */

    public void testApp()
    {
        /*
        Map<String, String> env = System.getenv();
        String db_url = env.get("DATABASE_URL");

        // Get a fully-configured connection to the database, or exit
        // immediately
        Database db = Database.getDatabase(db_url);
        if (db == null)
            assertTrue(false);
            
        int insertRow = 0;
        insertRow = db.insertMessageRow("ejscribner", "adding message from test");
        assertTrue(1==insertRow);
        //TODO: Add a delete row method
        //I dont have a delete row method so i cant delete the row i insert in test
        */
    }

}
