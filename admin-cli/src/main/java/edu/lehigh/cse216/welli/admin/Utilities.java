/*
    Created By: Elliot J Scribner on 2019-11-17
    Student ID: ejs320
    Lab #: **Num**
    Utilities: **Description**
 */

package edu.lehigh.cse216.welli.admin;

import org.apache.commons.lang.StringUtils;

import javax.swing.text.StyledEditorKit;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

public class Utilities {
    static String ERR_NULL = "NULLVAL";
    // this method will print out an array of PostData's
    public static void printTable(ArrayList<Database.PostData> data) {
        CommandLineTable postsTable = new CommandLineTable();
        postsTable.setHeaders("ID", "Username", "Title", "Body", "Breed", "Date", "Upvotes", "Downvotes");
        for (Database.PostData pd : data) {
            postsTable.addRow(Integer.toString(pd.mId), pd.mUsername, pd.mTitle, pd.mBody, pd.mBreed, pd.mDate, Integer.toString(pd.mUpvotes), Integer.toString(pd.mDownvotes));
        }
        System.out.println();
        postsTable.print();
        System.out.println();
    }

    // this method will print out a singular PostData
    public static void printTable(Database.PostData data) {
        CommandLineTable postsTable = new CommandLineTable();
        postsTable.setHeaders("ID", "Username", "Title", "Body", "Breed", "Date", "Upvotes", "Downvotes");
        postsTable.addRow(Integer.toString(data.mId), data.mUsername, data.mTitle, data.mBody, data.mBreed, data.mDate, Integer.toString(data.mUpvotes), Integer.toString(data.mDownvotes));
        System.out.println();
        postsTable.print();
        System.out.println();
    }


    public static void printFileTable(ArrayList<Database.FileData> data) {
        CommandLineTable fileTable = new CommandLineTable();
        fileTable.setHeaders("File ID", "Owner", "Name", "Type", "Url", "Created", "Modified", "Size", "Trashed");
        for (Database.FileData fd : data) {
            if(fd.fId == null)
                fd.fId = ERR_NULL;
            if(fd.fName == null)
                fd.fName = ERR_NULL;
            if(fd.fType == null)
                fd.fType = ERR_NULL;
            if(fd.fUrl == null)
                fd.fUrl = ERR_NULL;
            if(fd.fCreated == null)
                fd.fCreated = ERR_NULL;
            if(fd.fModified == null)
                fd.fModified = ERR_NULL;
            fileTable.addRow(fd.fId, Integer.toString(fd.fOwner), fd.fName, fd.fType, fd.fUrl, StringUtils.abbreviate(fd.fCreated, 13), StringUtils.abbreviate(fd.fModified, 13), Long.toString(fd.fSize), Boolean.toString(fd.fTrashed));
        }
        System.out.println();
        fileTable.print();
        System.out.println();
    }


    /**
     * Print the mainMenu for our program
     */
    static void mainMenu() {
        System.out.println("Main Menu");
        System.out.println("  [T] Create all (6) tables");
        System.out.println("  [D] Drop all (6) tables");
        System.out.println("  [1] Query for a specific row");
        System.out.println("  [*] Query for all rows");
        System.out.println("  [P] Insert a new post");
        System.out.println("  [F] View All Files");
        System.out.println("  [U] View Files for a User");
        System.out.println("  [q] Quit Program");
        System.out.println("  [?] Help (this message)");
    }

    /**
     * Ask the user to enter a mainMenu option; repeat until we get a valid option
     *
     * @param in A BufferedReader, for reading from the keyboard
     *
     * @return The character corresponding to the chosen mainMenu option
     */
    static char prompt(BufferedReader in) {
        // The valid actions:
        String actions = "TD1*PFUq?";

        // We repeat until a valid single-character option is selected
        while (true) {
            System.out.print("[" + actions + "] :> ");
            String action;
            try {
                action = in.readLine();
            } catch (IOException e) {
                e.printStackTrace();
                continue;
            }
            if (action.length() != 1)
                continue;
            if (actions.contains(action)) {
                return action.charAt(0);
            }
            System.out.println("Invalid Command");
        }
    }

    /**
     * Ask the user to enter a String message
     *
     * @param in A BufferedReader, for reading from the keyboard
     * @param message A message to display when asking for input
     *
     * @return The string that the user provided.  May be "".
     */
    static String getString(BufferedReader in, String message) {
        String s;
        try {

            System.out.println(message + " :> ");
            s = in.readLine();

        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
        return s;
    }

    /**
     * Ask the user to enter an integer
     *
     * @param in A BufferedReader, for reading from the keyboard
     * @param message A message to display when asking for input
     *
     * @return The integer that the user provided.  On error, it will be -1
     */
    static int getInt(BufferedReader in, String message) {
        int i = -1;
        try {
            System.out.println(message + " :> ");
            i = Integer.parseInt(in.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return i;
    }
}
