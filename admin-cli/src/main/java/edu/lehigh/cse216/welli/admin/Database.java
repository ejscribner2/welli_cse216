package edu.lehigh.cse216.welli.admin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.ArrayList;

public class Database {
    /**
     * The connection to the database.  When there is no connection, it should
     * be null.  Otherwise, there is a valid open connection
     */
    private Connection mConnection;

    /**
     * A prepared statement for getting all data in the database
     */
    private PreparedStatement mSelectAll;

    /**
     * A prepared statement for getting one row from the database
     */
    private PreparedStatement mSelectOne;

    /**
     * A prepared statement for all files
     */
    private PreparedStatement mSelectAllFiles;

    /**
     * A prepared statement for getting files for a specific user
     */
    private PreparedStatement mSelectFilesByUser;

    /**
     * A prepared statement for deleting a row from the database
     */
    //private PreparedStatement mDeleteOne;

    /**
     * A prepared statement for inserting into the database
     */
    private PreparedStatement mInsertOneUser;
    private PreparedStatement mInsertOnePost;
    private PreparedStatement mInsertOneVotes;

    /**
     * A prepared statement for updating a single row in the database
     */
    private PreparedStatement mUpdateOneUser;
    private PreparedStatement mUpdateOneMessage;
    private PreparedStatement mUpdatePassword;
    private PreparedStatement mGetSalt;
    //private PreparedStatement mUpdateOneVote;


    /**
     * Prepared statements for creating all 5 tables in our database
     */
    private PreparedStatement mCreateUsersTable;
    private PreparedStatement mCreatePostTable;
    private PreparedStatement mCreateFileTable;
    private PreparedStatement mCreateCommentTable;
    private PreparedStatement mCreateUpVotesTable;
    private PreparedStatement mCreateDownVotesTable;

    /**
     * Prepared statements for dropping all 5 tables in our database
     */
    private PreparedStatement mDropUsersTable;
    private PreparedStatement mDropPostTable;
    private PreparedStatement mDropFileTable;
    private PreparedStatement mDropCommentTable;
    private PreparedStatement mDropUpVotesTable;
    private PreparedStatement mDropDownVotesTable;

    /**
     * to add a vote we need to know what the current number of votes on that message is
     */
    private PreparedStatement mFindVotes;

    /** 
     * Validity checks
    */
    private PreparedStatement mValidUser;
    private PreparedStatement mValidUserByInt;
    private PreparedStatement mValidPost;


    /**
     * PostData is like a struct in C: we use it to hold data, and we allow
     * direct access to its fields.  In the context of this Database, PostData
     * represents the data we'd see in a row.
     * <p>
     * We make PostData a static class of Database because we don't really want
     * to encourage users to think of PostData as being anything other than an
     * abstract representation of a row of the database.  PostData and the
     * Database are tightly coupled: if one changes, the other should too.
     */
    public static class PostData {
        int mId;
        String mUsername;
        String mTitle;
        String mBody;
        String mBreed;
        String mDate;
        int mUpvotes;
        int mDownvotes;

        public PostData(int id, String username, String title, String body, String breed, String date, int upvotes, int downvotes) {
            mId = id;
            mUsername = username;
            mTitle = title;
            mBody = body;
            mBreed = breed;
            mDate = date;
            mUpvotes = upvotes;
            mDownvotes = downvotes;
        }
    }

    /**
     * Data Object for our Files
     */
    public static class FileData {
        String fId;
        int fOwner;
        String fName;
        String fType;
        String fUrl;
        String fCreated;
        String fModified;
        long fSize;
        Boolean fTrashed;

        public FileData(String id, int ownerId , String name , String type , String url , String created , String modified , long size , Boolean trashed) {
            fId = id;
            fOwner = ownerId;
            fName = name;
            fType = type;
            fUrl = url;
            fCreated = created;
            fModified = modified;
            fSize = size;
            fTrashed = trashed;
        }
    }

    /**
     * The Database constructor is private: we only create Database objects
     * through the getDatabase() method.
     */
    private Database() {
    }

    /**
     * Get a fully-configured connection to the database
     *
     * @param db_url The url the database server
     * @return A Database object, or null if we cannot connect properly
     */
    static Database getDatabase(String db_url) {
        // Create an un-configured Database object
        Database db = new Database();

        // Give the Database object a connection, fail if we cannot get one
        try {
            Class.forName("org.postgresql.Driver");
            URI dbUri = new URI(db_url);
            String username = dbUri.getUserInfo().split(":")[0];
            String password = dbUri.getUserInfo().split(":")[1];
            String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath() + "?sslmode=require";
            Connection conn = DriverManager.getConnection(dbUrl, username, password);
            if (conn == null) {
                System.err.println("Error: DriverManager.getConnection() returned a null object");
                return null;
            }
            db.mConnection = conn;
        } catch (SQLException e) {
            System.err.println("Error: DriverManager.getConnection() threw a SQLException");
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException cnfe) {
            System.out.println("Unable to find postgresql driver");
            return null;
        } catch (URISyntaxException s) {
            System.out.println("URI Syntax Error");
            return null;
        }

        // Attempt to create all of our prepared statements.  If any of these
        // fail, the whole getDatabase() call should fail
        try {
            // NB: we can easily get ourselves in trouble here by typing the
            //     SQL incorrectly.  We really should have things like "tblData"
            //     as constants, and then build the strings for the statements
            //     from those constants.

            // Note: no "IF NOT EXISTS" or "IF EXISTS" checks on table
            // creation/deletion, so multiple executions will cause an exception

            // CREATE tables ------------------------------------------------------------
            db.mCreateUsersTable = db.mConnection.prepareStatement(
                "CREATE TABLE chat_users\n" +
                        "(\n" +
                        "  u_id      SERIAL PRIMARY KEY,\n" +
                        "  username  VARCHAR(255),\n" +
                        "  firstname VARCHAR(255),\n" +
                        "  lastname  VARCHAR(255),\n" +
                        "  email     VARCHAR(255),\n" +
                        "  bio       VARCHAR(555)\n" +
                        ");"
                );
            db.mCreatePostTable = db.mConnection.prepareStatement(
                "CREATE TABLE post(\n" +
                    "p_id SERIAL PRIMARY KEY,\n" +
                    "sender_id INTEGER NOT NULL,\n" +
                    "title VARCHAR(50) NOT NULL,\n" +
                    "body VARCHAR(140) NOT NULL,\n" +
                    "breed VARCHAR(50) NOT NULL,\n" +
                    "date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,\n" +
                    "FOREIGN KEY (sender_id) REFERENCES chat_users(u_id)\n" + 
                            "ON DELETE CASCADE);"
            );
            db.mCreateFileTable = db.mConnection.prepareStatement(
                    "CREATE TABLE file\n" +
                            "(\n" +
                            "  file_id VARCHAR(255),\n" +
                            "  post_id INTEGER NOT NULL,\n" +
                            "  name VARCHAR(255),\n" +
                            "  type VARCHAR(255),\n" +
                            "  created_time TIMESTAMP,\n" +
                            "  modified_time TIMESTAMP,\n" +
                            "  size NUMERIC,\n" +
                            "  trashed BOOLEAN,\n" +
                            "  FOREIGN KEY (post_id) REFERENCES post (p_id)\n" +
                            "    ON DELETE CASCADE,\n" +
                            "  PRIMARY KEY (file_id, post_id)\n" +
                            ");"
            );
            db.mCreateCommentTable = db.mConnection.prepareStatement(
                "CREATE TABLE comment(\n" +
                    "c_id SERIAL PRIMARY KEY,\n" +
                    "p_id INTEGER NOT NULL,\n" +
                    "sender_id INTEGER NOT NULL,\n" +
                    "body VARCHAR(140),\n" +
                    "date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,\n" +
                    "FOREIGN KEY (sender_id) REFERENCES chat_users (u_id)\n" + 
                            "ON DELETE CASCADE,\n" +
                    "FOREIGN KEY (p_id) REFERENCES post (p_id));"
            );
            db.mCreateUpVotesTable = db.mConnection.prepareStatement(
                "CREATE TABLE upvotes(\n" +
                    "u_id INTEGER NOT NULL,\n" +
                    "p_id INTEGER NOT NULL,\n" +
                    "FOREIGN KEY (p_id) REFERENCES post(p_id)\n" +
                            "ON DELETE CASCADE,\n" +
                    "FOREIGN KEY (u_id) REFERENCES chat_users(u_id)\n" +
                            "ON DELETE CASCADE,\n" +
                    "PRIMARY KEY (u_id, p_id));"
            );
            db.mCreateDownVotesTable = db.mConnection.prepareStatement(
                "CREATE TABLE downvotes(\n" +
                    "u_id INTEGER NOT NULL,\n" +
                    "p_id INTEGER NOT NULL,\n" +
                    "FOREIGN KEY (p_id) REFERENCES post(p_id)\n" +
                            "ON DELETE CASCADE,\n" +
                    "FOREIGN KEY (u_id) REFERENCES chat_users(u_id)\n" +
                            "ON DELETE CASCADE,\n" +
                    "PRIMARY KEY (u_id, p_id));"
            );

            // DROP tables --------------------------------------------------------------
            db.mDropUsersTable = db.mConnection.prepareStatement("DROP TABLE chat_users");
            db.mDropPostTable = db.mConnection.prepareStatement("DROP TABLE post");
            db.mDropFileTable = db.mConnection.prepareStatement("DROP TABLE file");
            db.mDropCommentTable = db.mConnection.prepareStatement("DROP TABLE comment");
            db.mDropUpVotesTable = db.mConnection.prepareStatement("DROP TABLE upvotes");
            db.mDropDownVotesTable = db.mConnection.prepareStatement("DROP TABLE downvotes");

            // Standard CRUD operations -------------------------------------------------
            db.mSelectAll = db.mConnection.prepareStatement(
                "SELECT p_id as id, username, title, body, breed, date\n" +
                    ",(SELECT count(*) FROM upvotes WHERE post.p_id = upvotes.p_id) as upvotes\n" +
                    ",(SELECT count(*) FROM downvotes WHERE post.p_id = downvotes.p_id) as downvotes\n" +
                "FROM post LEFT JOIN chat_users ON chat_users.u_id = post.sender_id;"
            );
            db.mSelectOne = db.mConnection.prepareStatement(
                "SELECT p_id as id, username, title, body, breed, date\n" +
                    ",(SELECT count(*) FROM upvotes WHERE post.p_id = upvotes.p_id) as upvotes\n" +
                    ",(SELECT count(*) FROM downvotes WHERE post.p_id = downvotes.p_id) as downvotes\n" +
                "FROM post LEFT JOIN chat_users ON chat_users.u_id = post.sender_id\n" +
                "WHERE p_id = ?;"
            );

            db.mSelectAllFiles = db.mConnection.prepareStatement(
                    "select file_id,\n" +
                            "       sender_id as file_owner,\n" +
                            "       name,\n" +
                            "       type,\n" +
                            "       url,\n" +
                            "       created_time,\n" +
                            "       modified_time,\n" +
                            "       size,\n" +
                            "       trashed\n" +
                            " from file\n" +
                            "inner join post p on file.post_id = p.p_id;"
            );

            db.mSelectFilesByUser = db.mConnection.prepareStatement(
                    "select file_id,\n" +
                            "       sender_id as file_owner,\n" +
                            "       name,\n" +
                            "       type,\n" +
                            "       url,\n" +
                            "       created_time,\n" +
                            "       modified_time,\n" +
                            "       size,\n" +
                            "       trashed\n" +
                            " from file\n" +
                            "inner join post p on file.post_id = p.p_id where sender_id = ?;"
            );

            // NB: The null value below can be changed to a 'default' message string or a field a user enters when they sign up
            db.mInsertOneUser = db.mConnection.prepareStatement("INSERT INTO chat_users VALUES(default, ?, ?, ?, ?, null, ?, ?)");
            db.mInsertOnePost = db.mConnection.prepareStatement("INSERT INTO post VALUES(default, ?, ?, ?, default, ?)");

            db.mUpdatePassword = db.mConnection.prepareStatement("UPDATE chat_users SET passHash = ? WHERE u_id = ?");
            db.mGetSalt = db.mConnection.prepareStatement("SELECT salt FROM chat_users WHERE u_id = ?");
            
            // Validity checks ----------------------------------------------------------
            db.mValidUser = db.mConnection.prepareStatement(
                "SELECT u_id FROM chat_users WHERE username = ?;"
            );
            db.mValidUserByInt = db.mConnection.prepareStatement(
                "SELECT u_id FROM chat_users WHERE u_id = ?;"
            );
            db.mValidPost = db.mConnection.prepareStatement(
                "SELECT p_id FROM message WHERE p_id = ?;"
            );

        } catch (SQLException e) {
            System.err.println("Error creating prepared statement");
            e.printStackTrace();
            db.disconnect();
            return null;
        }
        return db;
    }

    /**
     * Close the current connection to the database, if one exists.
     * <p>
     * NB: The connection will always be null after this call, even if an
     * error occurred during the closing operation.
     *
     * @return True if the connection was cleanly closed, false otherwise
     */
    boolean disconnect() {
        if (mConnection == null) {
            System.err.println("Unable to close connection: Connection was null");
            return false;
        }
        try {
            mConnection.close();
        } catch (SQLException e) {
            System.err.println("Error: Connection.close() threw a SQLException");
            e.printStackTrace();
            mConnection = null;
            return false;
        }
        mConnection = null;
        return true;
    }

    /**
     * Create all 6 tables.  If they already exist, this will print an error
     */
    void createTable() {
        try {
            mCreateUsersTable.execute();
            mCreatePostTable.execute();
            mCreateFileTable.execute();
            mCreateCommentTable.execute();
            mCreateUpVotesTable.execute();
            mCreateDownVotesTable.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("Success.");
    }

    /**
     * Remove all 6 tables from the database.  If they do not exist, this will print
     * an error.
     */
    void dropTable() { 
        try {
            mDropDownVotesTable.execute();
            mDropUpVotesTable.execute();
            mDropCommentTable.execute();
            mDropFileTable.execute();
            mDropPostTable.execute();
            mDropUsersTable.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("Success.");
    }

    /**
     * Query the database for a list of all subjects and their IDs
     *
     * @return All rows, as an ArrayList
     */
    ArrayList<PostData> selectAll() {
        ArrayList<PostData> res = new ArrayList<PostData>();
        try {
            ResultSet rs = mSelectAll.executeQuery();
            while (rs.next()) {
                res.add(
                    new PostData(
                        rs.getInt("id"), 
                        rs.getString("username"), 
                        rs.getString("title"), 
                        rs.getString("body"), 
                        rs.getString("breed"), 
                        rs.getString("date"), 
                        rs.getInt("upvotes"), 
                        rs.getInt("downvotes")
                    )
                );
            }
            rs.close();
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }



    /**
     * Get all data for a specific row, by ID
     *
     * @param id The id of the row being requested
     * @return The data for the requested row, or null if the ID was invalid
     */
    PostData selectOne(int id) {
        PostData res = null;
        try {
            mSelectOne.setInt(1, id);
            ResultSet rs = mSelectOne.executeQuery();
            if (rs.next()) {
                res = new PostData(
                    rs.getInt("id"), 
                    rs.getString("username"), 
                    rs.getString("title"), 
                    rs.getString("body"), 
                    rs.getString("breed"), 
                    rs.getString("date"), 
                    rs.getInt("upvotes"), 
                    rs.getInt("downvotes")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    ArrayList<FileData> selectAllFiles() {
        ArrayList<FileData> res = new ArrayList<FileData>();
        try {
            ResultSet rs = mSelectAllFiles.executeQuery();
            while(rs.next()) {
                res.add(
                        new FileData(
                        rs.getString("file_id"),
                        rs.getInt("file_owner"),
                        rs.getString("name"),
                        rs.getString("type"),
                        rs.getString("url"),
                        rs.getString("created_time"),
                        rs.getString("modified_time"),
                        rs.getLong("size"),
                        rs.getBoolean("trashed")
                        )
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return res;
    }

    ArrayList<FileData> selectFilesByUser(int userToQuery) {
        ArrayList<FileData> res = new ArrayList<FileData>();
        try {
            mSelectFilesByUser.setInt(1, userToQuery);
            ResultSet rs = mSelectFilesByUser.executeQuery();
            while(rs.next()) {
                res.add(
                        new FileData(
                                rs.getString("file_id"),
                                rs.getInt("file_owner"),
                                rs.getString("name"),
                                rs.getString("type"),
                                rs.getString("url"),
                                rs.getString("created_time"),
                                rs.getString("modified_time"),
                                rs.getLong("size"),
                                rs.getBoolean("trashed")
                        )
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return res;
    }

    int insertUserRow(String username, String firstname, String lastname, String email, String salt, String passHash) {
        int count = 0;
        
        try {
            mValidUser.setString(1, username);
            ResultSet rs = mValidUser.executeQuery();
            if (!rs.next()) {
                mInsertOneUser.setString(1, username);
                mInsertOneUser.setString(2, firstname);
                mInsertOneUser.setString(3, lastname);
                mInsertOneUser.setString(4, email);
                mInsertOneUser.setString(5, salt);
                mInsertOneUser.setString(6, passHash);
                count += mInsertOneUser.executeUpdate();
            } else {
                System.err.println("Looks like that user already exists.");
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    int insertPostRow(int sender_id, String title, String body, String breed) {
        int count = 0;
        try {
            mInsertOnePost.setInt(1, sender_id);
            mInsertOnePost.setString(2, title);
            mInsertOnePost.setString(3, body);
            mInsertOnePost.setString(4, breed);
            count += mInsertOnePost.executeUpdate();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public int updatePassword(int id, String newPassword) {
        int res = -1;
        try {
            // Check if user exists and get their salt
            mGetSalt.setInt(1, id);
            ResultSet rs = mGetSalt.executeQuery();
            if (rs.next()) {
                String newPassHash = BCrypt.hashpw(newPassword, rs.getString("salt"));
                mUpdatePassword.setString(1, newPassHash);
                mUpdatePassword.setInt(2, id);
                res = mUpdatePassword.executeUpdate();
            }
            else {
                System.err.println("No user with id " + id);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

	public int updateOneUser(int id, String newUsername) {
        //check if the user exists
        int res = -1;
        int exists = 0;
        try {
            mValidUserByInt.setInt(1, id);
            ResultSet rs = mValidUserByInt.executeQuery();
            if(rs.next()){
                exists = rs.getInt("u_id");
            }
            if(exists != 0){
                mUpdateOneUser.setString(1, newUsername);
                mUpdateOneUser.setInt(2, id);
                res = mUpdateOneUser.executeUpdate();
            } else{
                System.err.println("Looks like that userID doesn't exist, so you can't update the username");
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;	
	}
}