package edu.lehigh.cse216.welli.admin;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Map;


import static edu.lehigh.cse216.welli.admin.Utilities.*;

/**
 * App is our basic admin app.  For now, it is a demonstration of the six key 
 * operations on a database: connect, insert, update, query, delete, disconnect
 */
public class App {


    /**
     * The main routine runs a loop that gets a request from the user and
     * processes it
     * 
     * @param argv Command-line options.  Ignored by this program.
     */
    public static void main(String[] argv) throws IOException {
        // get the Postgres configuration from the environment
        Map<String, String> env = System.getenv();
        String db_url = env.get("DATABASE_URL");

        // Get a fully-configured connection to the database, or exit
        // immediately
        Database db = Database.getDatabase(db_url);
        if (db == null)
            return;

        // Start our basic command-line interpreter:
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        mainMenu();
        while (true) {
            try {
            // NB: for better testability, each action should be a separate
            //     function call
            char action = prompt(in);
            int res;
                switch (action) {
                    case 'q' :
                        db.disconnect();
                        return;
                    case '?' :
                        mainMenu();
                        break;
                    case 'T' :
                        db.createTable();
                        break;
                    case 'D' :
                        db.dropTable();
                        break;
                    case 'P' :  /* Add a post */
                        int sender_id = getInt(in, "sender_id");
                        String title = getString(in, "title");
                        String body = getString(in, "body");
                        String breed = getString(in, "breed");
                        res = db.insertPostRow(sender_id, title, body, breed);
                        System.out.println(res + " rows added.");
                        break;
                    case '*' : /* Get all posts */
                        ArrayList<Database.PostData> allPostsResult = db.selectAll();
                        if (allPostsResult.size() < 1) {
                            System.out.println("No posts");
                            break;
                        }
                        printTable(allPostsResult);
                        break;
                    case '1' : /* Get one post */
                        int pid = getInt(in, "Enter post id");
                        Database.PostData singleUser = db.selectOne(pid);
                        if (singleUser == null) {
                            System.out.println("No post exists with that id");
                            break;
                        }
                        printTable(singleUser);
                        break;
                    case 'F' : /* Get all files */
                        ArrayList<Database.FileData> allFilesResult = db.selectAllFiles();
                        if (allFilesResult.size() < 1) {
                            System.out.println("No files");
                            break;
                        }
                        printFileTable(allFilesResult);
                        break;
                    case 'U' : /* Get files by user */
                        int userToQuery = getInt(in, "Which users files would you like to see?");
                        ArrayList<Database.FileData> userFilesResult = db.selectFilesByUser(userToQuery);
                        if (userFilesResult.size() < 1) {
                            System.out.println("No files");
                            break;
                        }
                        printFileTable(userFilesResult);
                        break;
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                db.disconnect();
            }
        }
    }

    static int manageContent(Database db, BufferedReader in) {
        while (true) {
            try {
                // NB: for better testability, each action should be a separate
                //     function call
                char action = prompt(in);
                switch (action) {
                    case 'q' :
                        db.disconnect();
                        return -1;

                    case 'b' :
                        return 0;
                }
            }
            catch (Exception e) {
                db.disconnect();
            }
        }
    }
}